<?php
/*
Plugin Name: WP User Frontend Pro
Plugin URI: https://wedevs.com/wp-user-frontend-pro/
Description: The paid module to add extra features on WP User Frontend.
Author: weDevs
Version: 2.5
Author URI: https://wedevs.com
License: GPL2
TextDomain: wpuf-pro
Domain Path: /languages/
*/

class WP_User_Frontend_Pro {
    /**
     * Class constructor.
     */
    public function __construct() {
        if ( ! class_exists( 'WP_User_Frontend' ) ) {
            if ( ! current_user_can( 'manage_options' ) ) {
                return;
            }

            add_action( 'admin_notices', array( $this, 'wpuf_activation_notice' ) );
            add_action( 'wp_ajax_wpuf_pro_install_wp_user_frontend', array( $this, 'install_wp_user_frontend' ) );
            return;
        }

        // Define constants
        $this->define_constants();

        // Include files
        $this->includes();

        // Instantiate classes
        $this->instantiate();

        // Initialize the action hooks
        $this->init_actions();
    }

    /**
     * Initializes the WP_User_Frontend_Pro() class
     *
     * Checks for an existing WeDevs_WeDevs_Dokan() instance
     * and if it doesn't find one, creates it.
     */
    public static function init() {
        static $instance = false;

        if ( ! $instance ) {
            $instance = new WP_User_Frontend_Pro();
        }

        return $instance;
    }

    /**
     * Show wordpress error notice if WP User Frontend not found
     *
     * @since 2.4.2
     *
     */
    public function wpuf_activation_notice() {
        ?>
        <div class="updated" id="wpuf-pro-installer-notice" style="padding: 1em; position: relative;">
            <h2><?php _e( 'Your WP User Frontend Pro is almost ready!', 'wpuf-pro' ); ?></h2>

            <?php
                $plugin_file      = basename( dirname( __FILE__ ) ) . '/wpuf-pro.php';
                $core_plugin_file = 'wp-user-frontend/wpuf.php';
            ?>
            <a href="<?php echo wp_nonce_url( 'plugins.php?action=deactivate&amp;plugin=' . $plugin_file . '&amp;plugin_status=all&amp;paged=1&amp;s=', 'deactivate-plugin_' . $plugin_file ); ?>" class="notice-dismiss" style="text-decoration: none;" title="<?php _e( 'Dismiss this notice', 'wpuf-pro' ); ?>"></a>

            <?php if ( file_exists( WP_PLUGIN_DIR . '/' . $core_plugin_file ) && is_plugin_inactive( 'wpuf-user-frontend' ) ): ?>
                <p><?php _e( 'You just need to activate the Core Plugin to make it functional.', 'wpuf-pro' ); ?></p>
                <p>
                    <a class="button button-primary" href="<?php echo wp_nonce_url( 'plugins.php?action=activate&amp;plugin=' . $core_plugin_file . '&amp;plugin_status=all&amp;paged=1&amp;s=', 'activate-plugin_' . $core_plugin_file ); ?>"  title="<?php _e( 'Activate this plugin', 'wpuf-pro' ); ?>"><?php _e( 'Activate', 'wpuf-pro' ); ?></a>
                </p>
            <?php else: ?>
                <p><?php echo sprintf( __( "You just need to install the %sCore Plugin%s to make it functional.", "wpuf-pro" ), '<a target="_blank" href="https://wordpress.org/plugins/wp-user-frontend/">', '</a>' ); ?></p>

                <p>
                    <button id="wpuf-pro-installer" class="button"><?php _e( 'Install Now', 'wpuf-pro' ); ?></button>
                </p>
            <?php endif; ?>

        </div>

        <script type="text/javascript">
            (function ($) {
                $('#wpuf-pro-installer-notice #wpuf-pro-installer').click( function (e) {
                    e.preventDefault();
                    $(this).addClass('install-now updating-message');
                    $(this).text('<?php echo esc_js( 'Installing...', 'wpuf-pro' ); ?>');

                    var data = {
                        action: 'wpuf_pro_install_wp_user_frontend',
                        _wpnonce: '<?php echo wp_create_nonce('wpuf-pro-installer-nonce'); ?>'
                    };

                    $.post(ajaxurl, data, function (response) {
                        if (response.success) {
                            $('#wpuf-pro-installer-notice #wpuf-pro-installer').attr('disabled', 'disabled');
                            $('#wpuf-pro-installer-notice #wpuf-pro-installer').removeClass('install-now updating-message');
                            $('#wpuf-pro-installer-notice #wpuf-pro-installer').text('<?php echo esc_js( 'Installed', 'wpuf-pro' ); ?>');
                        }
                    });
                });
            })(jQuery);
        </script>
        <?php
    }

    /**
     * Install the WP User Frontend plugin via ajax
     *
     * @since 2.4.2
     *
     * @return json
     */
    public function install_wp_user_frontend() {

        if ( ! isset( $_REQUEST['_wpnonce'] ) || ! wp_verify_nonce( $_REQUEST['_wpnonce'], 'wpuf-pro-installer-nonce' ) ) {
            wp_send_json_error( __( 'Error: Nonce verification failed', 'wpuf-pro' ) );
        }

        include_once ABSPATH . 'wp-admin/includes/plugin-install.php';
        include_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';

        $plugin = 'wp-user-frontend';
        $api    = plugins_api( 'plugin_information', array( 'slug' => $plugin, 'fields' => array( 'sections' => false ) ) );

        $upgrader = new Plugin_Upgrader( new WP_Ajax_Upgrader_Skin() );
        $result   = $upgrader->install( $api->download_link );
        if ( is_wp_error( $result ) ) {
            wp_send_json_error( $result );
        }

        $result = activate_plugin( 'wp-user-frontend/wpuf.php' );
        if ( is_wp_error( $result ) ) {
            wp_send_json_error( $result );
        }

        wp_send_json_success();
    }

    /**
     * Define the constants
     *
     * @return void
     */
    private function define_constants() {
        define( 'WPUF_PRO_VERSION', '2.5' );
        define( 'WPUF_PRO_FILE', __FILE__ );
        define( 'WPUF_PRO_ROOT', dirname( __FILE__ ) );
        define( 'WPUF_PRO_INCLUDES', WPUF_PRO_ROOT . '/includes' );
        define( 'WPUF_PRO_ROOT_URI', plugins_url( '', __FILE__ ) );
        define( 'WPUF_PRO_ASSET_URI', WPUF_PRO_ROOT_URI . '/assets' );
    }

    /**
     * Include the files
     *
     * @return void
     */
    public function includes() {
        require_once WPUF_PRO_INCLUDES . '/login.php';
        require_once WPUF_PRO_INCLUDES . '/frontend-form-profile.php';
        require_once WPUF_PRO_INCLUDES . '/updates.php';

        if ( is_admin() ) {
            require_once WPUF_PRO_ROOT . '/admin/coupon.php';
            require_once WPUF_PRO_ROOT . '/admin/coupon-element.php';
            require_once WPUF_PRO_ROOT . '/admin/posting-profile.php';
            require_once WPUF_PRO_ROOT . '/admin/template-profile.php';
            require_once WPUF_PRO_ROOT . '/admin/pro-page-installer.php';
            require_once WPUF_PRO_ROOT . '/admin/profile-forms-list-table.php';
        }

        //class files to include pro elements
        require_once WPUF_PRO_INCLUDES . '/form.php';
        require_once WPUF_PRO_INCLUDES . '/profile-form.php';
        require_once WPUF_PRO_INCLUDES . '/form-element.php';
        require_once WPUF_PRO_INCLUDES . '/content-restriction.php';
        require_once WPUF_PRO_INCLUDES . '/subscription.php';
        require_once WPUF_PRO_INCLUDES . '/coupons.php';
        require_once WPUF_PRO_INCLUDES . '/render-form.php';
    }

    /**
     * Instantiate the classes
     *
     * @return void
     */
    public function instantiate(){
        WPUF_Login::init();
        new WPUF_Frontend_Form_Profile();
        new WPUF_Content_Restriction();

        if ( is_admin() ) {
            new WPUF_Updates();
            new WPUF_Admin_Form_Pro();
            new WPUF_Admin_Profile_Form_Pro();
            new WPUF_Admin_Posting_Profile();
            new WPUF_Admin_Coupon();
            WPUF_Coupons::init();
        }
    }

    /**
     * Init the action/filter hooks
     *
     * @return void
     */
    public function init_actions() {
        add_action( 'wpuf_form_buttons_custom', array( $this, 'wpuf_form_buttons_custom_runner' ) );
        add_action( 'wpuf_form_buttons_other', array( $this, 'wpuf_form_buttons_other_runner') );
        add_action( 'wpuf_form_post_expiration', array( $this, 'wpuf_form_post_expiration_runner') );
        add_action( 'wpuf_form_setting', array( $this, 'form_setting_runner' ),10,2 );
        add_action( 'wpuf_form_settings_post_notification', array( $this, 'post_notification_hook_runner') );
        add_action( 'wpuf_edit_form_area_profile', array( $this, 'wpuf_edit_form_area_profile_runner' ) );
        add_action( 'wpuf_add_profile_form_top', array( $this, 'wpuf_add_profile_form_top_runner' ), 10, 2 );
        add_action( 'registration_setting' , array($this,'registration_setting_runner') );
        add_action( 'wpuf_check_post_type' , array( $this, 'wpuf_check_post_type_runner' ),10,2 );
        add_action( 'wpuf_form_custom_taxonomies', array( $this, 'wpuf_form_custom_taxonomies_runner' ) );
        add_action( 'wpuf_conditional_field_render_hook', array( $this, 'wpuf_conditional_field_render_hook_runner' ), 10, 3 );
        add_filter( 'wpuf-get-form-fields', array( $this, 'get_form_fields' ) );

        //subscription
        add_action( 'wpuf_admin_subscription_detail', array($this, 'wpuf_admin_subscription_detail_runner'), 10, 4 );
        add_action( 'wpuf_update_subscription_pack', array( $this, 'wpuf_update_subscription_pack_runner' ), 10, 2 );
        add_filter( 'wpuf_get_subscription_meta' , array( $this, 'wpuf_get_subscription_meta_runner' ),10, 2 ) ;

        add_filter( 'wpuf_new_subscription', array( $this, 'wpuf_new_subscription_runner' ), 10, 4 );

        //coupon
        add_action( 'wpuf_coupon_settings_form', array($this, 'wpuf_coupon_settings_form_runner'),10,1 );
        add_action( 'wpuf_check_save_permission', array($this, 'wpuf_check_save_permission_runner'),10,2 );

        //render_form
        add_action( 'wpuf_render_pro_repeat', array( $this, 'wpuf_render_pro_repeat_runner' ),10,7 );
        add_action( 'wpuf_render_pro_date', array( $this, 'wpuf_render_pro_date_runner' ),10,7 );
        add_action( 'wpuf_render_pro_file_upload', array( $this, 'wpuf_render_pro_file_upload_runner' ),10,7 );
        add_action( 'wpuf_render_pro_map', array( $this, 'wpuf_render_pro_map_runner' ),10,7 );
        add_action( 'wpuf_render_pro_country_list', array( $this, 'wpuf_render_pro_country_list_runner' ),10,7 );
        add_action( 'wpuf_render_pro_numeric_text', array( $this, 'wpuf_render_pro_numeric_text_runner' ),10,7 );
        add_action( 'wpuf_render_pro_address', array( $this, 'wpuf_render_pro_address_runner' ),10,7 );
        add_action( 'wpuf_render_pro_step_start', array( $this, 'wpuf_render_pro_step_start_runner' ),10,9 );
        add_action( 'wpuf_render_pro_recaptcha', array( $this, 'wpuf_render_pro_recaptcha_runner' ),10,9 );
        add_action( 'wpuf_render_pro_really_simple_captcha', array( $this, 'wpuf_render_pro_really_simple_captcha_runner' ),10,9 );
        add_action( 'wpuf_render_pro_action_hook', array( $this, 'wpuf_render_pro_action_hook_runner' ),10,9 );
        add_action( 'wpuf_render_pro_toc', array( $this, 'wpuf_render_pro_toc_runner' ),10,9 );

        add_action( 'wpuf_render_pro_ratings', array( $this, 'wpuf_render_pro_ratings_runner' ),10,9 );

        //render element form in backend form builder
        add_action('wpuf_admin_field_custom_repeater',array($this,'wpuf_admin_field_custom_repeater_runner'),10,4);
        add_action('wpuf_admin_template_post_repeat_field',array($this,'wpuf_admin_template_post_repeat_field_runner'),10,5);
        add_action('wpuf_admin_field_custom_date',array($this,'wpuf_admin_field_custom_date_runner'),10,4);
        add_action('wpuf_admin_template_post_date_field',array($this,'wpuf_admin_template_post_date_field_runner'),10,5);
        // add_action('wpuf_admin_template_post_image_upload',array($this,'wpuf_admin_template_post_image_upload_runner'),10,5);
        add_action('wpuf_admin_field_custom_file',array($this,'wpuf_admin_field_custom_file_runner'),10,4);
        add_action('wpuf_admin_template_post_file_upload',array($this,'wpuf_admin_template_post_file_upload_runner'),10,5);
        add_action('wpuf_admin_field_custom_map',array($this,'wpuf_admin_field_custom_map_runner'),10,4);
        add_action('wpuf_admin_template_post_google_map',array($this,'wpuf_admin_template_post_google_map_runner'),10,5);
        add_action('wpuf_admin_field_country_select',array($this,'wpuf_admin_field_country_select_runner'),10,4);
        add_action('wpuf_admin_template_post_country_list_field',array($this,'wpuf_admin_template_post_country_list_field_runner'),10,5);
        add_action('wpuf_admin_field_numeric_field',array($this,'wpuf_admin_field_numeric_field_runner'),10,4);
        add_action('wpuf_admin_template_post_numeric_text_field',array($this,'wpuf_admin_template_post_numeric_text_field_runner'),10,5);
        add_action('wpuf_admin_field_address_field',array($this,'wpuf_admin_field_address_field_runner'),10,4);
        add_action('wpuf_admin_template_post_address_field',array($this,'wpuf_admin_template_post_address_field_runner'),10,5);
        add_action('wpuf_admin_field_step_start',array($this,'wpuf_admin_field_step_start_runner'),10,4);
        add_action('wpuf_admin_template_post_step_start',array($this,'wpuf_admin_template_post_step_start_runner'),10,5);
        add_action('wpuf_admin_field_recaptcha',array($this,'wpuf_admin_field_recaptcha_runner'),10,4);
        add_action('wpuf_admin_template_post_recaptcha',array($this,'wpuf_admin_template_post_recaptcha_runner'),10,5);
        add_action('wpuf_admin_field_really_simple_captcha',array($this,'wpuf_admin_field_really_simple_captcha_runner'),10,4);
        add_action('wpuf_admin_template_post_really_simple_captcha',array($this,'wpuf_admin_template_post_really_simple_captcha_runner'),10,5);
        add_action('wpuf_admin_field_action_hook',array($this,'wpuf_admin_field_action_hook_runner'),10,4);
        add_action('wpuf_admin_template_post_action_hook',array($this,'wpuf_admin_template_post_action_hook_runner'),10,5);
        add_action('wpuf_admin_field_toc',array($this,'wpuf_admin_field_toc_runner'),10,4);
        add_action('wpuf_admin_field_ratings',array($this,'wpuf_admin_field_ratings_runner'),10,4);

        add_action('wpuf_admin_template_post_toc',array($this,'wpuf_admin_template_post_toc_runner'),10,5);
        add_action('wpuf_admin_template_post_ratings',array($this,'wpuf_admin_template_post_ratings'),10,5);

        // admin menu
        add_action( 'wpuf_admin_menu_top', array($this, 'admin_menu_top') );
        add_action( 'wpuf_admin_menu', array($this, 'admin_menu') );

        //render_form
        add_action( 'wpuf_add_post_form_top', array($this, 'wpuf_add_post_form_top_runner'),10,2 );
        add_action( 'wpuf_edit_post_form_top', array($this, 'wpuf_edit_post_form_top_runner'),10,3 );

        //page install
        add_filter( 'wpuf_pro_page_install' , array( $this, 'install_pro_pages' ), 10, 1 );

        //show custom html in frontend
        add_filter( 'wpuf_custom_field_render', array( $this, 'render_custom_fields' ), 99, 4 );

        //delete post
        add_action( 'trash_post', array( $this, 'delete_post_function' ) );

        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts_styles' ) );

        // post form templates
        add_action( 'wpuf_get_post_form_templates', array($this, 'post_form_templates') );
    }

    function admin_menu_top() {
        $capability = wpuf_admin_role();

        $profile_forms_page = add_submenu_page( 'wp-user-frontend', __( 'Registration Forms', 'wpuf' ), __( 'Registration Forms' ), $capability, 'wpuf-profile-forms', array( $this, 'wpuf_profile_forms_page' ) );
        add_action( "load-$profile_forms_page", array( $this, 'footer_styles' ) );
    }

    /**
     * Callback method for Profile Forms submenu
     *
     * @since 2.5
     *
     * @return void
     */
    function wpuf_profile_forms_page() {
        $action = isset( $_GET['action'] ) ? $_GET['action'] : null;
        $add_new_page_url = admin_url( 'admin.php?page=wpuf-profile-forms&action=add-new' );

        switch ( $action ) {
            case 'edit':
                require_once WPUF_PRO_ROOT . '/views/profile-form.php';
                break;

            case 'add-new':
                require_once WPUF_PRO_ROOT . '/views/profile-form.php';
                break;

            default:
                require_once WPUF_PRO_ROOT . '/admin/profile-forms-list-table-view.php';
                break;
        }
    }

    function footer_styles() {
        echo '<style type="text/css">
            .column-user_role { width:12% !important; overflow:hidden }
        </style>';
    }

    function admin_menu() {
        $capability = wpuf_admin_role();

        add_submenu_page( 'wp-user-frontend', __( 'Coupons', 'wpuf-pro' ), __( 'Coupons', 'wpuf-pro' ), $capability, 'edit.php?post_type=wpuf_coupon' );
    }

    public function wpuf_form_buttons_custom_runner() {
        //add formbuilder widget pro buttons
        WPUF_form_element::add_form_custom_buttons();
    }

    public function wpuf_form_buttons_other_runner() {
        WPUF_form_element::add_form_other_buttons();
    }

    public function wpuf_form_post_expiration_runner(){
        WPUF_form_element::render_form_expiration_tab();
    }

    public function form_setting_runner( $form_settings, $post ) {
        WPUF_form_element::add_form_settings_content( $form_settings, $post );
    }

    public function post_notification_hook_runner() {
        WPUF_form_element::add_post_notification_content();
    }

    public function wpuf_edit_form_area_profile_runner() {
        WPUF_form_element::render_registration_form();
    }

    public function registration_setting_runner() {
        WPUF_form_element::render_registration_settings();
    }

    public function wpuf_check_post_type_runner( $post, $update ) {
        WPUF_form_element::check_post_type( $post, $update );
    }

    public function wpuf_form_custom_taxonomies_runner() {
        WPUF_form_element::render_custom_taxonomies_element();
    }

    public function wpuf_conditional_field_render_hook_runner( $field_id, $con_fields, $obj ) {
        WPUF_form_element::render_conditional_field( $field_id, $con_fields, $obj );
    }

    //subscription
    public function wpuf_admin_subscription_detail_runner( $sub_meta, $hidden_recurring_class, $hidden_trial_class, $obj ) {
        WPUF_subscription_element::add_subscription_element( $sub_meta, $hidden_recurring_class, $hidden_trial_class, $obj );
    }

    //coupon
    public function wpuf_coupon_settings_form_runner( $obj ) {
        WPUF_Coupon_Elements::add_coupon_elements( $obj );
    }

    public function wpuf_check_save_permission_runner( $post, $update ) {
        WPUF_Coupon_Elements::check_saving_capability( $post, $update );
    }

    //render_form
    public function wpuf_render_pro_repeat_runner( $form_field, $post_id, $type, $form_id, $form_settings, $classname, $obj ) {
        WPUF_render_form_element::repeat( $form_field, $post_id, $type, $form_id, $classname, $obj );
        $obj->conditional_logic( $form_field, $form_id );
    }
    public function wpuf_render_pro_date_runner( $form_field, $post_id, $type, $form_id, $form_settings, $classname, $obj ){
        WPUF_render_form_element::date( $form_field, $post_id, $type, $form_id, $obj );
        $obj->conditional_logic( $form_field, $form_id );
    }

    public function wpuf_render_pro_file_upload_runner( $form_field, $post_id, $type, $form_id, $form_settings, $classname, $obj ){
        WPUF_render_form_element::file_upload( $form_field, $post_id, $type, $form_id, $obj );
        $obj->conditional_logic( $form_field, $form_id );
    }

    public function wpuf_render_pro_map_runner( $form_field, $post_id, $type, $form_id, $form_settings, $classname, $obj ) {
        WPUF_render_form_element::map( $form_field, $post_id, $type, $form_id, $classname, $obj );
        $obj->conditional_logic( $form_field, $form_id );
    }
    public function wpuf_render_pro_country_list_runner( $form_field, $post_id, $type, $form_id, $form_settings, $classname, $obj ){
        WPUF_render_form_element::country_list( $form_field, $post_id, $type, $form_id, $classname, $obj );
        $obj->conditional_logic( $form_field, $form_id );
    }
    public function wpuf_render_pro_numeric_text_runner( $form_field, $post_id, $type, $form_id, $form_settings, $classname, $obj ){
        WPUF_render_form_element::numeric_text( $form_field, $post_id, $type, $form_id, $classname, $obj );
        $obj->conditional_logic( $form_field, $form_id );
    }
    public function wpuf_render_pro_address_runner( $form_field, $post_id, $type, $form_id, $form_settings, $classname, $obj ){
        WPUF_render_form_element::address_field( $form_field, $post_id, $type, $form_id, $classname, $obj );
        $obj->conditional_logic( $form_field, $form_id );
    }
    public function wpuf_render_pro_step_start_runner( $form_field, $post_id, $type, $form_id, $form_settings, $classname, $obj, $multiform_start, $enable_multistep ) {
        WPUF_render_form_element::step_start( $form_field, $post_id, $type, $form_id, $multiform_start, $enable_multistep, $obj );
        $obj->conditional_logic( $form_field, $form_id );
    }
    public function wpuf_render_pro_recaptcha_runner( $form_field, $post_id, $type, $form_id, $multiform_start, $enable_multistep, $obj ){
        $form_field['name'] = 'recaptcha';
        WPUF_render_form_element::recaptcha( $form_field, $post_id, $form_id);
        $obj->conditional_logic( $form_field, $form_id );
    }
    public function wpuf_render_pro_really_simple_captcha_runner( $form_field, $post_id, $type, $form_id, $multiform_start, $enable_multistep, $obj ){
        $form_field['name'] = 'really_simple_captcha';
        WPUF_render_form_element::really_simple_captcha( $form_field, $post_id, $form_id );
        $obj->conditional_logic( $form_field, $form_id );
    }
    public function wpuf_render_pro_action_hook_runner( $form_field, $post_id, $type, $form_id, $form_settings, $classname, $obj ){
        WPUF_render_form_element::action_hook( $form_field, $form_id, $post_id,  $form_settings );
        $obj->conditional_logic( $form_field, $form_id );
    }
    public function wpuf_render_pro_toc_runner( $form_field, $post_id, $type, $form_id, $multiform_start, $enable_multistep, $obj ){
        WPUF_render_form_element::toc( $form_field, $post_id, $form_id );
        $obj->conditional_logic( $form_field, $form_id );
    }

    public function wpuf_render_pro_ratings_runner( $form_field, $post_id, $type, $form_id, $multiform_start, $enable_multistep, $obj ) {
        WPUF_render_form_element::ratings( $form_field, $post_id, $form_id );
        $obj->conditional_logic( $form_field, $form_id );
    }

    //form element's rendering form in backend form builder
    public function wpuf_admin_field_custom_repeater_runner( $type, $field_id, $classname, $obj ) {
       WPUF_form_element::repeat_field( $field_id, 'Custom field: Repeat Field',$classname );
    }
    public function wpuf_admin_template_post_repeat_field_runner( $name, $count, $input_field, $classname, $obj ){
        WPUF_form_element::repeat_field( $count, $name, $classname, $input_field );
    }

    public function wpuf_admin_field_custom_date_runner( $type, $field_id, $classname, $obj ){
        WPUF_form_element::date_field( $field_id, 'Custom Field: Date',$classname );
    }
    public function wpuf_admin_template_post_date_field_runner( $name, $count, $input_field, $classname, $obj ){
        WPUF_form_element::date_field( $count, $name, $classname, $input_field );
    }

    public function wpuf_admin_template_post_image_upload_runner( $name, $count, $input_field, $classname, $obj ){
        WPUF_form_element::image_upload( $count, $name, $classname, $input_field );
    }

    public function wpuf_admin_field_custom_file_runner( $type, $field_id, $classname, $obj ){
        WPUF_form_element::file_upload( $field_id, 'Custom field: File Upload', $classname);
    }
    public function wpuf_admin_template_post_file_upload_runner( $name, $count, $input_field, $classname, $obj ){
        WPUF_form_element::file_upload( $count, $name, $classname, $input_field );
    }

    public function wpuf_admin_field_custom_map_runner( $type, $field_id, $classname, $obj ){
        WPUF_form_element::google_map( $field_id, 'Custom Field: Google Map',$classname );
    }
    public function wpuf_admin_template_post_google_map_runner( $name, $count, $input_field, $classname, $obj ){
        WPUF_form_element::google_map( $count, $name, $classname, $input_field );
    }

    public function wpuf_admin_field_country_select_runner( $type, $field_id, $classname, $obj ){
        WPUF_form_element::country_list_field( $field_id, 'Custom field: Select', $classname );
    }
    public function wpuf_admin_template_post_country_list_field_runner( $name, $count, $input_field, $classname, $obj ) {
        WPUF_form_element::country_list_field( $count, $name, $classname, $input_field );
    }

    public function wpuf_admin_field_numeric_field_runner( $type, $field_id, $classname, $obj ){
        WPUF_form_element::numeric_text_field( $field_id, 'Custom field: Numeric Text', $classname);
    }
    public function wpuf_admin_template_post_numeric_text_field_runner( $name, $count, $input_field, $classname, $obj ){
        WPUF_form_element::numeric_text_field( $count, $name, $classname, $input_field );
    }

    public function wpuf_admin_field_address_field_runner( $type, $field_id, $classname, $obj ){
        WPUF_form_element::address_field( $field_id, 'Custom field: Address',$classname);
    }
    public function wpuf_admin_template_post_address_field_runner( $name, $count, $input_field, $classname, $obj ){
        WPUF_form_element::address_field( $count, $name, $classname, $input_field );
    }

    public function wpuf_admin_field_step_start_runner( $type, $field_id, $classname, $obj ){
        WPUF_form_element::step_start( $field_id, 'Step Starts', $classname);
    }
    public function wpuf_admin_template_post_step_start_runner( $name, $count, $input_field, $classname, $obj ){
        WPUF_form_element::step_start( $count, $name, $classname, $input_field );
    }

    public function wpuf_admin_field_recaptcha_runner( $type, $field_id, $classname, $obj ){
        WPUF_form_element::recaptcha( $field_id, 'reCaptcha', $classname );
    }
    public function wpuf_admin_template_post_recaptcha_runner( $name, $count, $input_field, $classname, $obj ){
        WPUF_form_element::recaptcha( $count, $name, $classname, $input_field );
    }

    public function wpuf_admin_field_really_simple_captcha_runner( $type, $field_id, $classname, $obj ){
        WPUF_form_element::really_simple_captcha( $field_id, 'Really Simple Captcha',$classname );
    }
    public function wpuf_admin_template_post_really_simple_captcha_runner( $name, $count, $input_field, $classname, $obj ){
        WPUF_form_element::really_simple_captcha( $count, $name, $classname, $input_field );
    }

    public function wpuf_admin_field_action_hook_runner( $type, $field_id, $classname, $obj ){
        WPUF_form_element::action_hook( $field_id, 'Action Hook', $classname );
    }
    public function wpuf_admin_template_post_action_hook_runner( $name, $count, $input_field, $classname, $obj ){
        WPUF_form_element::action_hook( $count, $name, $classname, $input_field );
    }

    public function wpuf_admin_field_toc_runner( $type, $field_id, $classname, $obj ){
        WPUF_form_element::toc( $field_id, 'TOC', $classname );
    }

    public function wpuf_admin_field_ratings_runner( $type, $field_id, $classname, $obj ){
        WPUF_form_element::ratings( $field_id, 'Ratings', $classname );
    }

    public function wpuf_admin_template_post_toc_runner( $name, $count, $input_field, $classname, $obj ){
        WPUF_form_element::toc( $count, $name, $classname, $input_field );
    }

    public function wpuf_admin_template_post_ratings( $name, $count, $input_field, $classname, $obj ){
        WPUF_form_element::ratings( $count, $name, $classname, $input_field );
    }

    public function wpuf_add_profile_form_top_runner( $form_id, $form_settings ) {
        if ( isset( $form_settings['multistep_progressbar_type'] ) && $form_settings['multistep_progressbar_type'] == 'progressive' ) {
            wp_enqueue_script('jquery-ui-progressbar');
        }
    }

    //render_form
    public function wpuf_add_post_form_top_runner( $form_id, $form_settings ) {
        if ( ! isset( $form_settings['enable_multistep'] ) || $form_settings['enable_multistep'] != 'yes' ) {
            return;
        }

        if ( $form_settings['multistep_progressbar_type'] == 'progressive' ) {
            wp_enqueue_script('jquery-ui-progressbar');
        }
    }

    public function wpuf_edit_post_form_top_runner( $form_id, $post_id, $form_settings ) {

        if ( ! isset( $form_settings['enable_multistep'] ) || $form_settings['enable_multistep'] != 'yes' ) {
            return;
        }

        if ( isset( $form_settings['multistep_progressbar_type'] ) && $form_settings['multistep_progressbar_type'] == 'progressive' ) {
            wp_enqueue_script('jquery-ui-progressbar');
        }
    }

    //install pro version page
    function install_pro_pages( $profile_options ) {
        $wpuf_pro_page_installer = new wpuf_pro_page_installer();
        return $wpuf_pro_page_installer->install_pro_version_pages( $profile_options );
    }


    //update data of pro feature of subscription
    function wpuf_update_subscription_pack_runner( $subscription_id, $post ) {
        WPUF_subscription_element::update_subcription_data( $subscription_id, $post );
    }

    //get subscription meta data
    function wpuf_get_subscription_meta_runner( $meta, $subscription_id ) {
        return WPUF_subscription_element::get_subscription_metadata( $meta, $subscription_id );
    }


    /**
     * update meta of user from the data of pack he has been assigned to
     *
     * @param $subscription_id
     * @param $post
     *
     * @return string
     */
    function wpuf_new_subscription_runner( $user_meta, $user_id, $pack_id, $recurring ) {
        return WPUF_subscription_element::set_subscription_meta_to_user( $user_meta, $user_id, $pack_id, $recurring );
    }

    function delete_post_function( $post_id ) {
        WPUF_subscription_element::restore_post_numbers( $post_id );
    }

    /**
     * show custom html
     */
    function render_custom_fields( $html, $value, $attr, $form_settings ) {

        switch( $attr['input_type']) {
            case 'ratings':
                $ratings_html = '';

                $ratings_html .= '<select name="'.$attr['name'].'" class="wpuf-ratings">';
                    foreach( $attr['options'] as $key => $option ) :
                        $ratings_html .= '<option value="'.$key.'" ' .( in_array( $key, $value ) ? 'selected' : '' ) . '>' .$option. '</option>';
                    endforeach;
                $ratings_html .= '</select>';

                $html .= '<li><label>' . $attr['label'] . ': </label> ';
                $html .= ' '.$ratings_html.'</li>';

                $js = '<script type="text/javascript">';
                    $js .= 'jQuery(function($) {';
                        $js .= '$(".wpuf-ratings").barrating({';
                            $js .= 'theme: "css-stars",';
                            $js .= 'readonly: true';
                        $js .= '});';
                    $js .= '});';
                $js .= '</script>';

                $html .= $js;
                break;

            case 'repeat':
                $multiple = ( isset( $attr['multiple'] ) && $attr['multiple'] == 'true' ) ? true : false;

                if ( ! $multiple ) {
                    $value = isset( $value['0'] ) ? $value['0'] : '';
                    $value = explode( WPUF_Render_Form::$separator, $value );
                    $html .= sprintf( '<li><label>%s</label>: %s</li>', $attr['label'], implode( ', ', $value ) );
                }

                break;
        }

        return $html;
    }

    /**
     * Enqueue scripts and styles
     */
    function enqueue_scripts_styles( $hook ) {
        global $post;

        if ( ! $post ) {
            return;
        }

        if ( wpuf_has_shortcode( 'wpuf_form', $post->ID )
            || wpuf_has_shortcode( 'wpuf_edit', $post->ID )
            || wpuf_has_shortcode( 'wpuf_profile', $post->ID ) || is_single() ) {

            wp_enqueue_style( 'wpuf-rating-star-css', WPUF_PRO_ASSET_URI . '/css/css-stars.css' );
            wp_enqueue_script( 'wpuf-rating-js', WPUF_PRO_ASSET_URI . '/js/jquery.barrating.min.js', array( 'jquery' ) );
        }
    }

    /**
     * Post form templates
     *
     * @since 2.4
     *
     * @param  array $integrations
     *
     * @return array
     */
    public function post_form_templates( $integrations ) {
        require_once WPUF_PRO_INCLUDES . '/post-form-templates/woocommerce.php';

        $integrations['WPUF_Post_Form_Template_WooCommerce'] = new WPUF_Post_Form_Template_WooCommerce();

        return $integrations;
    }

    /**
     * Filter form fields
     *
     * @since 2.5
     *
     * @param array $field
     *
     * @return array
     */
    public function get_form_fields( $field ) {
        // make sure that country_list has all its properties
        if ( 'country_list' === $field['input_type'] ) {

            if ( ! isset( $field['country_list']['country_select_hide_list'] ) ) {
                $field['country_list']['country_select_hide_list'] = array();
            }

            if ( ! isset( $field['country_list']['country_select_show_list'] ) ) {
                $field['country_list']['country_select_show_list'] = array();
            }
        }

        if ( 'address' === $field['input_type'] ) {
            if ( ! isset( $field['address']['country_select']['country_select_hide_list'] ) ) {
                $field['address']['country_select']['country_select_hide_list'] = array();
            }

            if ( ! isset( $field['address']['country_select']['country_select_show_list'] ) ) {
                $field['address']['country_select']['country_select_show_list'] = array();
            }
        }

        if ( 'recaptcha' === $field['input_type'] && ! isset( $filed['enable_no_captcha'] ) ) {
            $field['enable_no_captcha'] = '';
        }

        if ( 'toc' === $field['input_type'] && ! isset( $field['show_checkbox'] ) ) {
            $field['show_checkbox'] = false;
        }

        if ( 'ratings' === $field['input_type'] && ! isset( $field['selected'] ) ) {
            $field['selected'] = array();
        }

        return $field;
    }
}

/**
 * Load WPUF Pro Plugin when all plugins loaded
 *
 * @return void
 */
function wpuf_pro_load_plugin() {
    WP_User_Frontend_Pro::init();
}

add_action( 'plugins_loaded', 'wpuf_pro_load_plugin' );
