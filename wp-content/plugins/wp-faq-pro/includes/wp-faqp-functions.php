<?php
/**
 * Functions File
 *
 * @package WP FAQ Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Update default settings
 * 
 * @package WP FAQ Pro
 * @since 1.1.7
 */
function wp_faqp_default_settings() {
	
	global $wp_faqp_options;
	
	$wp_faqp_options = array(
						'enable_woo_faq'	=> '',
						'woo_faq_tab_text'	=> __('FAQ', 'sp-faq'),
						'custom_css' 		=> '',
					);
	
	$default_options = apply_filters('wp_faqp_options_default_values', $wp_faqp_options );
	
	// Update default options
	update_option( 'wp_faqp_options', $default_options );
	
	// Overwrite global variable when option is update
	$wp_faqp_options = wp_faqp_get_settings();
}

/**
 * Get Settings From Option Page
 * Handles to return all settings value
 * 
 * @package WP FAQ Pro
 * @since 1.1.7
 */
function wp_faqp_get_settings() {
	
	$options = get_option('wp_faqp_options');
	
	$settings = is_array($options) 	? $options : array();
	
	return $settings;
}

/**
 * Get an option
 * Looks to see if the specified setting exists, returns default if not
 * 
 * @package WP FAQ Pro
 * @since 1.1.7
 */
function wp_faqp_get_option( $key = '', $default = false ) {
	global $wp_faqp_options;

	$value = ! empty( $wp_faqp_options[ $key ] ) ? $wp_faqp_options[ $key ] : $default;
	$value = apply_filters( 'wp_faqp_get_option', $value, $key, $default );
	return apply_filters( 'wp_faqp_get_option_' . $key, $value, $key, $default );
}

/**
 * Escape Tags & Slashes
 * Handles escapping the slashes and tags
 *
 * @package WP FAQ Pro
 * @since 1.1.7
 */
function wp_faqp_esc_attr($data) {
	return esc_attr( stripslashes($data) );
}

/**
 * Strip Slashes From Array
 *
 * @package WP FAQ Pro
 * @since 1.1.7
 */
function wp_faqp_slashes_deep($data = array(), $flag = false) {
	
	if($flag != true) {
		$data = wp_faqp_nohtml_kses($data);
	}
	$data = stripslashes_deep($data);
	return $data;
}

/**
 * Strip Html Tags 
 * 
 * It will sanitize text input (strip html tags, and escape characters)
 * 
 * @package WP FAQ Pro
 * @since 1.1.7
 */
function wp_faqp_nohtml_kses($data = array()) {
	
	if ( is_array($data) ) {
		
		$data = array_map('wp_faqp_nohtml_kses', $data);
		
	} elseif ( is_string( $data ) ) {
		$data = trim( $data );
		$data = wp_filter_nohtml_kses($data);
	}
	
	return $data;
}

/**
 * Function to unique number value
 * 
 * @package WP FAQ Pro
 * @since 1.1.3
 */
function wp_faqp_get_unique() {
	static $unique = 0;
	$unique++;
	return $unique;
}

/**
 * Function to add array after specific key
 * 
 * @package WP FAQ Pro
 * @since 1.1.5
 */
function wp_faqp_add_array(&$array, $value, $index) {

	if( is_array($array) && is_array($value) ){
		$split_arr  = array_splice($array, max(0, $index));
		$array      = array_merge( $array, $value, $split_arr);
	}
	return $array;
}

/**
 * Function to get grid column based on grid
 * 
 * @package WP FAQ Pro
 * @since 1.0.0
 */
function wp_faqp_grid_column( $grid = '' ) {

	if($grid == '2') {
		$grid_clmn = '6';
	} else if($grid == '3') {
		$grid_clmn = '4';
	}  else if($grid == '4') {
		$grid_clmn = '3';
	} else if ($grid == '1') {
		$grid_clmn = '12';
	} else {
		$grid_clmn = '12';
	}
	
	return $grid_clmn;
}

/**
 * Function to get `sp_faq` shortcode designs
 * 
 * @package WP FAQ Pro
 * @since 1.0.0
 */
function wp_faqp_designs() {
	$design_arr = array(
						'design-1'	=> __('Design 1', 'sp-faq'),
						'design-2'	=> __('Design 2', 'sp-faq'),
						'design-3'	=> __('Design 3', 'sp-faq'),
						'design-4'	=> __('Design 4', 'sp-faq'),
						'design-5'	=> __('Design 5', 'sp-faq'),
						'design-6'	=> __('Design 6', 'sp-faq'),
						'design-7'	=> __('Design 7', 'sp-faq'),
						'design-8'	=> __('Design 8', 'sp-faq'),
						'design-9'	=> __('Design 9', 'sp-faq'),
						'design-10'	=> __('Design 10', 'sp-faq'),
						'design-11'	=> __('Design 11', 'sp-faq'),
						'design-12'	=> __('Design 12', 'sp-faq'),
						'design-13'	=> __('Design 13', 'sp-faq'),
						'design-14'	=> __('Design 14', 'sp-faq'),
						'design-15'	=> __('Design 15', 'sp-faq'),
					);
	return apply_filters('wp_faqp_designs', $design_arr );
}