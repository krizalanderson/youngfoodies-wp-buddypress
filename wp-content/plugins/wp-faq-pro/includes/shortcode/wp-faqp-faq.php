<?php
/**
 * Shortcode File
 *
 * Handles the 'sp_faq' shortcode of plugin
 *
 * @package WP FAQ Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * 'sp_faq' shortcode
 * 
 * @package WP FAQ Pro
 * @since 1.0.0
 */
function wp_faqp_shortcode( $atts, $content = null ) {

	// Shortcode Parameter
	extract(shortcode_atts(array(
		'limit' 			=> '20',
		'category' 			=> '',
		'include_cat_child'	=> 'true',
		'design'   			=> 'design-1',
		'category_name' 	=> '',
		'single_open'   	=> 'true',
		'default_open_faq'	=> '',
		'transition_speed' 	=> '300',
		'background_color' 	=> '',
		'font_color' 		=> '',
		'border_color' 		=> '',
		'heading_font_size' => '',
		'active_bg_color'	=> '',
		'active_font_color'	=> '',
		'active_title_color'=> '',
		'icon_color' 		=> 'black',
		'icon_type' 		=> 'arrow',
		'icon_position' 	=> 'right',
		'order'				=> 'desc',
		'orderby'			=> 'date',
		'exclude_cat'		=> array(),
		'exclude_post'		=> array(),
		'posts'				=> array(),
		), $atts));

	// Enqueing required script
	wp_enqueue_script( 'wp-faqp-public-script' );

	$unique				= wp_faqp_get_unique();
	$shortcode_designs 	= wp_faqp_designs();
	$limit				= !empty($limit) 		? $limit 	: '20';
	$category 			= (!empty($category))	? explode(',',$category) : '';
	$include_cat_child	= ( $include_cat_child == 'false' ) ? false : true;
	$design 			= ($design && (array_key_exists(trim($design), $shortcode_designs))) ? trim($design) 	: 'design-1';
	$category_name		= !empty($category_name) 		? $category_name 		: '';
	$single_open		= !empty($single_open) 			? $single_open 			: 'true';
	$transition_speed	= !empty($transition_speed) 	? $transition_speed 	: '300';
	$background_color	= !empty($background_color) 	? $background_color 	: '';
	$font_color			= !empty($font_color) 			? $font_color 			: '';
	$border_color		= !empty($border_color) 		? $border_color 		: '';
	$heading_font_size	= !empty($heading_font_size) 	? $heading_font_size 	: '18';
	$icon_color_cls		= ($icon_color == 'white') 		? 'wp-faqp-white' 		: 'wp-faqp-black';
	$icon_type_cls		= ($icon_type == 'plus') 		? 'wp-faqp-plus' 		: 'wp-faqp-arrow';
	$icon_position_cls	= ($icon_position == 'left') 	? 'wp-faqp-left-icon' 	: 'wp-faqp-right-icon';
	$order 				= (strtolower($order) == 'asc') ? 'ASC' 	: 'DESC';
	$orderby			= !empty($orderby) 				? $orderby 	: 'date';
	$exclude_cat 		= !empty($exclude_cat)		? explode(',', $exclude_cat) 	: array();
	$exclude_post 		= !empty($exclude_post)		? explode(',', $exclude_post) 	: array();
	$posts 				= !empty($posts)			? explode(',', $posts) 			: array();

	// FAQ Configuration
	$faq_conf = compact( 'single_open', 'transition_speed' );

	// Taking some globals
	global $post;

	// Taking some variable
	$loop_count = 1;

	// WP Query Parameter
	$args = array (
				'post_type'			=> WP_FAQP_POST_TYPE,
				'post_status'		=> array( 'publish' ),
				'orderby'			=> $orderby,
				'order'				=> $order,
				'posts_per_page' 	=> $limit,
				'post__not_in'		=> $exclude_post,
				'post__in'			=> $posts,
			);

	// Category Parameter
	if($category != "") {

		$args['tax_query'] = array(
								array(
										'taxonomy' 			=> WP_FAQP_CAT,
										'field' 			=> 'term_id',
										'terms' 			=> $category,
										'include_children'	=> $include_cat_child,
									));

	} else if( !empty($exclude_cat) ) {
		
		$args['tax_query'] = array(
									array(
										'taxonomy' 			=> WP_FAQP_CAT,
										'field' 			=> 'term_id',
										'terms' 			=> $exclude_cat,
										'operator'			=> 'NOT IN',
										'include_children'	=> $include_cat_child,
									));
	}

	// WP Query
	$query 		= new WP_Query($args);
	$post_count = $query->post_count;

	ob_start();

	// If post is there
	if( $query->have_posts() ) {

		// FAQ unique style
		$style = '';
		$style .= "<style type='text/css'>
						.faq-accordion .wp-faqp-accordion-{$unique} .faq-main{";
					
							if( !empty($background_color) ) {
								$style .= "background:{$background_color};";
							}
							if( !empty($border_color) ) {
								$style .= "border:1px solid {$border_color};";
							}
		$style	.=		"} ";
		$style 	.=		".faq-accordion .wp-faqp-accordion-{$unique} .faq-main h4{";
							
							if( !empty($font_color) ) {
								$style .= "color:{$font_color};";
							}
							if( !empty($heading_font_size) ) {
								$style .= "font-size:{$heading_font_size}px !important;";
							}
		$style	.=		"} ";
		$style 	.=		".faq-accordion .wp-faqp-accordion-{$unique} .faq-main.wp-faqp-open{";
							
							if( !empty($active_bg_color) ) {
								$style .= "background:{$active_bg_color};";
							}
							if( !empty($active_font_color) ) {
								$style .= "color:{$active_font_color};";
							}
							
		$style	.=		"} ";
		$style 	.=		".faq-accordion .wp-faqp-accordion-{$unique} .faq-main.wp-faqp-open h4{";
							
							if( !empty($active_title_color) ) {
								$style .= "color:{$active_title_color};";
							}

		$style	.=		"} ";
		$style	.= "</style>";

		echo $style; // echo FAQ style
	?>

		<div class="faq-accordion wp-faqp-accordion-list wp-faqp-<?php echo $design; ?> <?php echo $icon_color_cls.' '.$icon_type_cls; ?> wp-faqp-clearfix" data-accordion-group>
			
			<?php if($category_name != '') { ?>
			<h2 class="wp-faqp-cat-title"><?php echo $category_name; ?></h2>
			<?php } ?>

			<div class="wp-faqp-accordion-<?php echo $unique; ?> wp-faqp-main-wrp <?php echo $icon_position_cls; ?>" id="wp-faqp-accordion-<?php echo $unique; ?>" data-conf="<?php echo htmlspecialchars(json_encode($faq_conf)); ?>">

			<?php while ($query->have_posts()) : $query->the_post();
				$faq_open_cls = ($default_open_faq >= $loop_count) ? 'wp-faqp-open' : '';
			?>

				<div class="faq-main <?php echo $faq_open_cls; ?>" data-accordion>
					<div class="faq-title" data-control>
						<h4><?php the_title(); ?></h4>
					</div>
					
					<div class="wp-faqp-cnt-wrp" data-content>
						<div class="wp-faqp-cnt-inr-wrp">
							<?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) {
								the_post_thumbnail('thumbnail'); 
							} ?>
							<div class="faq-content"><?php the_content(); ?></div>
						</div>
					</div>
				</div>
				
			<?php
				$loop_count++;
			endwhile;
			?>
			
			</div>
		</div>

	<?php } // End of check have post

	wp_reset_query(); // Reset WP Query

	$content .= ob_get_clean();
	return $content;
}

// 'sp_faq' shortcode
add_shortcode('sp_faq', 'wp_faqp_shortcode');