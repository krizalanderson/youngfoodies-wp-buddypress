<?php
/**
 * Script Class
 *
 * Handles the script and style functionality of plugin
 *
 * @package WP FAQ Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class Wp_Faqp_Script {
	
	function __construct() {

		// Action to add style at front side
		add_action( 'wp_enqueue_scripts', array( $this, 'wp_faqp_front_style') );

		// Action to add script at front side
		add_action( 'wp_enqueue_scripts', array( $this, 'wp_faqp_front_script') );

		// Action to add style in backend
		add_action( 'admin_enqueue_scripts', array($this, 'wp_faqp_admin_style') );

		// Action to add script at admin side
		add_action( 'admin_enqueue_scripts', array($this, 'wp_faqp_admin_scripts') );

		// Action to add custom css in head
		add_action( 'wp_head', array($this, 'wp_faqp_custom_css'), 20 );
	}

	/**
	 * Function to add style at front side
	 * 
	 * @package WP FAQ Pro
 	 * @since 1.0.0
	 */
	function wp_faqp_front_style() {
		
		// Registring public style
		wp_register_style( 'wp-faqp-public-style', WP_FAQP_URL.'assets/css/wp-faqp-public.css', null, WP_FAQP_VERSION );
		wp_enqueue_style('wp-faqp-public-style');
	}

	/**
	 * Function to add script at front side
	 * 
	 * @package WP FAQ Pro
 	 * @since 1.0.0
	 */
	function wp_faqp_front_script() {

		// Enqueue built in script
		wp_enqueue_script( 'jquery' );

		// Registring public script
		wp_register_script( 'wp-faqp-public-script', WP_FAQP_URL.'assets/js/wp-faqp-public.js', array('jquery'), WP_FAQP_VERSION, true );
	}

	/**
	 * Enqueue admin styles
	 * 
	 * @package WP FAQ Pro
	 * @since 1.2.1
	 */
	function wp_faqp_admin_style( $hook ) {

		global $typenow;

		// Pages array
		$pages_array = array( WP_FAQP_POST_TYPE );
		
		// If page is plugin setting page then enqueue script
		if( in_array($typenow, $pages_array) ) {
			
			// Registring admin script
			wp_register_style( 'wp-faqp-admin-style', WP_FAQP_URL.'assets/css/wp-faqp-admin.css', null, WP_FAQP_VERSION );
			wp_enqueue_style( 'wp-faqp-admin-style' );
		}
	}

	/**
	 * Function to add script at admin side
	 * 
	 * @package WP FAQ Pro
 	 * @since 1.0.0
	 */
	function wp_faqp_admin_scripts() {
		
		global $wp_query, $post;
		
	    $screen       = get_current_screen();
	    $screen_id    = $screen ? $screen->id : '';
	    
	    // Product sorting - only when sorting by menu order on the products page
	    if ( $screen_id == 'edit-sp_faq' && isset( $wp_query->query['orderby'] ) && $wp_query->query['orderby'] == 'menu_order title' ) {
	        wp_register_script( 'wp-faqp-ordering', WP_FAQP_URL . 'assets/js/wp-faqp-ordering.js', array( 'jquery-ui-sortable' ), WP_FAQP_VERSION, true );
	        wp_enqueue_script( 'wp-faqp-ordering' );
	    }
	}

	/**
	 * Add custom css to head
	 * 
	 * @package WP FAQ Pro
	 * @since 1.1.7
	 */
	function wp_faqp_custom_css() {

		$custom_css = wp_faqp_get_option('custom_css');
		
		if( !empty($custom_css) ) {
			$css  = '<style type="text/css">' . "\n";
			$css .= $custom_css;
			$css .= "\n" . '</style>' . "\n";
			
			echo $css;
		}
	}
}

$wp_faqp_script = new Wp_Faqp_Script();