<?php
/**
 * Templating Functions
 *
 * @package WP FAQ Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * WooCommerce FAQ tab section HTML
 * 
 * @package WP FAQ Pro
 * @since 1.0.0
 */
function wp_faqp_product_faq_tab() {
	
	global $post;
	$prefix = WP_FAQP_META_PREFIX;

	$wp_faqp_enable 	= get_post_meta( $post->ID, $prefix.'enable', true );
	$wp_faqp_shortcode 	= get_post_meta( $post->ID, $prefix.'shortcode', true );

	if( $wp_faqp_enable && $wp_faqp_shortcode ) {
		echo do_shortcode( $wp_faqp_shortcode );
	}
}

/**
 * WooCommerce FAQ tab section HTML at Admin Side
 * 
 * @package WP FAQ Pro
 * @since 1.0.0
 */
function wp_faqp_woo_product_data_panels() { 

	global $post;

	$prefix = WP_FAQP_META_PREFIX;
?>
	
	<div id="wp_faqp_product_data" class="wp_faqp_product_data panel woocommerce_options_panel hidden">
		
		<div class="options_group reviews">
			<?php woocommerce_wp_checkbox( array( 'id' => $prefix.'enable', 'label' => __( 'Enable FAQ', 'sp-faq' ), 'cbvalue' => '1', 'description' => __('Check this box to enable faq on product page', 'sp-faq') ) ); ?>
		</div>
		
		<div class="options_group">
			<?php woocommerce_wp_textarea_input(  array( 'id' => $prefix.'shortcode', 'label' => __( 'FAQ Shortcode', 'sp-faq' ), 'desc_tip' => 'true', 'description' => __('Enter FAQ shortcode which you want to display.', 'sp-faq'), 'class' => 'wp-faqp-shortcode') ); ?>
		</div>

	</div><!-- end .wp_faqp_product_data -->

<?php
}