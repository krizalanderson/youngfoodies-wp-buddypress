<?php
/* This general search will find matches within event_name, event_notes, and the location_name, address, town, state and country. */ 
$args = !empty($args) ? $args:array(); /* @var $args array */ 
?>
<!-- START General Search -->

<style type="text/css">

input.em-events-search-text::-webkit-input-placeholder {
    font-size: 6px;
    line-height: 3;
}

</style>
<div class="em-search-text em-search-field">
	<script type="text/javascript">
	EM.search_term_placeholder = '<?php echo esc_attr($args['search_term_label']); ?>';
	</script>
	<label>
		<span class="screen-reader-text"><?php echo esc_html($args['search_term_label']); ?></span>
		<input type="text" name="em_search" class="em-events-search-text em-search-text" value="<?php echo esc_attr($args['search']); ?>" style="
    padding: 8px 10px 9px;
    font-size: 12px !important;
    color: #ddd !important;
    /* height: 40px; */
" /><button type="submit" id="" style="
    position: absolute;
    right: 5px;
    top: 25px;
    height: 41px;
    width: 40px;
    color: white;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
"><i class="bb-icon-search"></i></button>
	</label>
</div>
<!-- END General Search -->