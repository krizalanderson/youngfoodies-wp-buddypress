<?php
/**
 * Template Name: blog Page Template
 *
 */
get_header();
?>

<div id="primary" class="site-content">
    
	<div id="blog_id">
	<div class="drop_box_blog">
	<select class="selectpicker_blog select-order">
  <option value="_post_like_count">Sort by most liked</option>
  <option value="post_date" selected>Sort by most recent</option>
</select>

</div>
<div class="blog_post_sidebar">
<?php dynamic_sidebar( 'unique-sidebar-id' ); 
$categories = get_categories( array(
    'order' => 'ASC',
    'parent'  => 0,
	'hide-empty'=>0
) ); ?>
<h2 class="widgettitle">Filter by category</h2>
 <?php echo '<ul class="category-list">';
foreach ( $categories as $category ) {
    printf( '<li data-id="%d"><a href="javascript:void(0)">%s</a></li><br />', 
		$category->term_id,
        esc_html( $category->name )
    );
}
echo '</ul>';
?>


</div>
<div style="display:none;" class="loaderImage"><img src="/wp-content/uploads/2017/03/ajax-loader-1-1.gif"></div>
<div class="blogs_post_div" style="display:none;">
	<?php ajax_filter_get_posts(); 
	
	?>
</div>

	</div>

</div>
<script>
var ajax_url = '<?php echo admin_url( 'admin-ajax.php' ) ?>';

jQuery('.category-list li').on('click', function(){
        jQuery('.loaderImage').show();
	jQuery('.blogs_post_div').fadeOut();

	var order_by = jQuery('.select-order').val();
	var cat_id = jQuery(this).data('id')
	var paged_num = 1
	//console.log(cat_id);
	var data = {
			'action': 'catfilter_blog',
			'cat_id': cat_id,
			'paged_num':paged_num,
			'order_by':order_by
		};

		jQuery.post(ajax_url , data, function(response) {
			jQuery('.blogs_post_div').html(response); 
			jQuery('.loaderImage').hide();
			jQuery('.blogs_post_div').fadeIn();
			//console.log(response);
		});
})

jQuery(document).ready(function() {
jQuery( document ).on('click', 'a.page-numbers', function( event ) {
  event.preventDefault();
        jQuery('.loaderImage').show();
	jQuery('.blogs_post_div').fadeOut();

	var order_by = jQuery('.select-order').val();
	var cat_id = jQuery('.category-list li.active').data('id');
	var paged_num = jQuery(this).html();
	//console.log(cat_id);
	var data = {
			'action': 'catfilter_blog',
			'cat_id': cat_id,
			'paged_num':paged_num,
			'order_by':order_by
		};

		jQuery.post( ajax_url , data, function(response) {
			jQuery('.blogs_post_div').html(response); 
			jQuery('.loaderImage').hide();
			jQuery('.blogs_post_div').fadeIn();
			jQuery("html, body").animate({
            scrollTop: 0
        }, 600);
			//console.log(response);
		});
})
//start order
jQuery( document ).on('change', '.select-order', function() {
  //event.preventDefault();
    jQuery('.loaderImage').show();
	jQuery('.blogs_post_div').fadeOut();

	var order_by = jQuery('.select-order').val();
	var cat_id = jQuery('.category-list li.active').data('id');
	
	var data = {
			'action': 'catfilter_blog',
			'cat_id': cat_id,
			'paged_num':1,
			'order_by':order_by
		 
		};

		jQuery.post( ajax_url , data, function(response) {
			jQuery('.blogs_post_div').html(response); 
			jQuery('.loaderImage').hide();
			jQuery('.blogs_post_div').fadeIn();
			});
})

//end order

jQuery(".category-list li").click(function () {
    jQuery(".category-list li").removeClass("active");
     jQuery(this).addClass("active");   
});

 jQuery('[data-id="80"]').trigger('click'); 
 

});



</script>
<?php
get_footer();