<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage OneSocial Theme
 * @since OneSocial Theme 1.0.0
 */
get_header();
?>

<div id="primary" class="site-content">

	<div id="content" role="main">
	<?php if(is_user_logged_in()){
    if( function_exists('pmpro_hasMembershipLevel') && pmpro_hasMembershipLevel() ){
      global $current_user;
      $current_user->membership_level = pmpro_getMembershipLevelForUser($current_user->ID);
      $ML_id = $current_user->membership_level->ID;
    }
  } 
 
   $key_1_value = get_post_meta( get_the_ID(), 'page_levels-'.$ML_id , true );
  ?>
<div class="single-nav">
					<div class="previous_previews_blog">
					<div class="left_nav"><?php next_post_link('%link', '<span class="next_icon"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>newer</span>'); ?></div><h1 class="post-entry-title"><?php the_title();?></h1><div class="right_nav"> <?php previous_post_link('%link', '<span class="previous_icon"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>older</span>'); ?>
		</div>			
	</div>
		
	</div>
	
			<?php if($key_1_value) { ?>		
		<div class="single-left-sidebar">
		<div class="singal_left_top">
		<span>
		<?php while ( have_posts() ) : the_post(); ?>
			 <?php  echo get_avatar( get_the_author_meta( 'ID' ) , 40 ); ?></span>
			<span class="title"><?php   echo $author = get_the_author();  ?></span>

			<span> <?php echo  $pfx_date = get_the_date(); ?></span>
			<?php endwhile; // end of the loop. ?>
  </div></div>
<div class="blogs_post_div">
		<?php while ( have_posts() ) : the_post(); ?>


			<?php //get_template_part( 'template-parts/content', get_post_format() ); ?>
			<!-- <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php //the_post_thumbnail('large'); ?>
    </a>-->
			
		<?php	//echo wpautop( $post->post_content );
		the_content()
			
			?>
			
			
			<footer class="entry-meta">
				<div class="row">
					<div class="entry-tags col">
						<?php
						$terms = wp_get_post_tags( get_the_ID() );
						if ( $terms ) {
							?>
							<h3><?php _e( 'Tagged in', 'onesocial' ); ?></h3><?php
							foreach ( $terms as $t ) {
								echo '<a href="' . get_tag_link( $t->term_id ) . '">' . $t->name . '<span>' . $t->count . '</span></a>';
							}
						}
						?>
					</div>

                                <?php if ( get_post_status(get_the_ID()) == 'publish' ) { ?>
					<!-- /.entry-tags -->
					<div class="entry-share col">
						<?php
						if ( function_exists( 'get_simple_likes_button' )  && is_singular( 'post' ) ) {
							echo get_simple_likes_button( get_the_ID() );
						}
						?>

						<ul class="helper-links">

							<?php if ( function_exists( 'sap_get_bookmark_button' ) && is_singular( 'post' ) && is_user_logged_in() ) { ?>
								<li>
									<?php
									$button = sap_get_bookmark_button();

									if ( !empty( $button ) ) {
										echo $button;
									} else {
										?>
										<a id="bookmarkme" href="#siteLoginBox" class="bookmark-it onesocial-login-popup-link">
											<span class="fa bb-helper-icon fa-bookmark-o"></span>
											<span><?php _e( 'Bookmark', 'onesocial' ); ?></span>
										</a><?php
									}
									?>
								</li>
							<?php } ?>

							<?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ?>
								<li>
									<?php ADDTOANY_SHARE_SAVE_KIT( array( 'use_current_page' => true ) ); ?>
								</li><?php
							}
							?>
						</ul>
					</div>
					<!-- /.entry-share -->
                                <?php } ?>
				</div>

				<?php //edit_post_link( __( 'Edit', 'onesocial' ), '<span class="edit-link">', '</span>' );    ?>

			</footer><!-- .entry-meta -->
			
		
		<?php $post_status = get_post_status(get_the_ID());
                        if ( 'publish' == $post_status || 'private' == $post_status ) {
                            comments_template( '', true );
                        } ?>
                        
<?php endwhile; // end of the loop. ?>
		</div>
		
		<div class="blog_post_sidebar">

<span> <h2>Other posts</h2></span>
			<?php get_related_posts_thumbnails(); ?>
				
			</div>
		
<?php } else if ($ML_id==2){ echo '<h1 style="text-align:center;">'.'"This content is for Founder members only." '.'</h1>' ;} else {echo '<h1 style="text-align:center;">'.'"This content is for members only."'.'</h1>';} ?>
	</div><!-- #content -->
</div><!-- #primary -->

<?php
get_footer();
