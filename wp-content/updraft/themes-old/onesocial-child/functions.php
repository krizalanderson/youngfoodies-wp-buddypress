<?php
/**
 * @package OneSocial Child Theme
 * The parent theme functions are located at /onesocial/buddyboss-inc/theme-functions.php
 * Add your own functions in this file.
 */

/**
 * Sets up theme defaults
 *
 * @since OneSocial Child Theme 1.0.0
 */
function onesocial_child_theme_setup()
{
  /**
   * Makes child theme available for translation.
   * Translations can be added into the /languages/ directory.
   * Read more at: http://www.buddyboss.com/tutorials/language-translations/
   */

  // Translate text from the PARENT theme.
  load_theme_textdomain( 'onesocial', get_stylesheet_directory() . '/languages' );

  // Translate text from the CHILD theme only.
  // Change 'onesocial' instances in all child theme files to 'onesocial_child_theme'.
  // load_theme_textdomain( 'onesocial_child_theme', get_stylesheet_directory() . '/languages' );

}
add_action( 'after_setup_theme', 'onesocial_child_theme_setup' );

/**
 * Enqueues scripts and styles for child theme front-end.
 *
 * @since OneSocial Child Theme  1.0.0
 */
function onesocial_child_theme_scripts_styles()
{
  /**
   * Scripts and Styles loaded by the parent theme can be unloaded if needed
   * using wp_deregister_script or wp_deregister_style.
   *
   * See the WordPress Codex for more information about those functions:
   * http://codex.wordpress.org/Function_Reference/wp_deregister_script
   * http://codex.wordpress.org/Function_Reference/wp_deregister_style
   **/

  /*
   * Styles
   */
  //wp_enqueue_style( 'onesocial-child-custom', get_stylesheet_directory_uri().'/css/custom.css' );
    wp_enqueue_style( 'onesocial-child-custom', get_stylesheet_directory_uri().'/css/responsive.css' );
}
add_action( 'wp_enqueue_scripts', 'onesocial_child_theme_scripts_styles', 9999 );


/****************************** CUSTOM FUNCTIONS ******************************/
// if not site admin, restrict PMs to friends
function pp_check_message_recipients( $message_info ) {

	//  site admins are not restricted
	if ( is_super_admin() )
		return $message_info;
	
	$recipients = $message_info->recipients;

	$friend_ids = friends_get_friend_user_ids( bp_displayed_user_id() ); 

	$nf = 0; 
		
	foreach ( $recipients as $key => $recipient ) {

		if ( ! in_array( $recipient->user_id, $friend_ids ) ) 
			$nf++;

	}

	// if any recipients are not friends, remove everyone from the recipient's list
	if (  $nf > 0 ) 
		unset( $message_info->recipients );
		
	return $message_info;
}
add_action( 'messages_message_before_save', 'pp_check_message_recipients' );

//end//
Function bp_displayed_user_is_friend() 
{ global $bp; if ( bp_is_profile_component() || bp_is_member() ) { if ( ('is_friend' != BP_Friends_Friendship::check_is_friend( $bp->loggedin_user->id, $bp->displayed_user->id )) && (bp_loggedin_user_id() != bp_displayed_user_id()) ) if ( !is_super_admin( bp_loggedin_user_id() ) ) return true; } }

 $args = array(
	'name'          => __( 'blog_sidebar', 'theme_text_domain' ),
	'id'            => 'unique-sidebar-id',
	'description'   => '',
        'class'         => '',
	'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>' ); 

register_sidebar( $args );

/*
function get_excerpt($limit, $source = null){

    if($source == "content" ? ($excerpt = get_the_content()) : ($excerpt = get_the_excerpt()));
    $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $limit);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
    $excerpt = $excerpt.'... <a href="'.get_permalink($post->ID).'">more</a>';
    return $excerpt;
}

*/


add_action( 'wp_ajax_catfilter_blog', 'ajax_filter_get_posts' );

//content limit
 //add_filter("the_content", "plugin_myContentFilter");

  //function plugin_myContentFilter($content)
  //{
    // Take the existing content and return a subset of it
   // return substr($content, 0, 300);
  //}

// Script for getting posts
function ajax_filter_get_posts() {
$user_package_id;
if(is_user_logged_in()){
    if( function_exists('pmpro_hasMembershipLevel') && pmpro_hasMembershipLevel() ){
      global $current_user;
      $current_user->membership_level = pmpro_getMembershipLevelForUser($current_user->ID);
      $user_package_id = $current_user->membership_level->ID;
    }
} 
$args = array( 
			'post_type'=> 'post', 
			'paged'=>1 , 
			'orderby' => 'post_date',  
			'order'    => 'DSC' 
		);

		$args['meta_query'] = array(
		array(
					'key' => 'page_levels-'.$user_package_id,
					'value' => $user_package_id,
					'compare' => 'EXISTS' 
	));

if(isset($_POST['paged_num'])){
	$args['paged'] = $_POST['paged_num'];
}
if(isset($_POST['cat_id'])){
	$args['cat'] = $_POST['cat_id'];
}
if(isset($_POST['order_by']) && $_POST['order_by'] == '_post_like_count'){
	$args['orderby'] = 'meta_value_num';
	$args['meta_key']= $_POST['order_by'];
}

//print_r($args);
query_posts( $args );
 
// The Loop
if(is_category( '80' ))
	{
echo '<style>.post-categories li:first-child{display:block;}</style>' ;
	}
	else{echo '<style>.post-categories li:first-child{display:none !important;}</style>' ;}
while ( have_posts() ) : the_post(); ?>
<div class="inner_post">

<?php if ( has_post_thumbnail() ) : ?>
    <a class="blog_post_thumb" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php the_post_thumbnail(); ?>
    </a>
<?php endif; ?>
<span class="author_blog"> 
<?php //$category =get_the_category(); ?> 
 
 <?php //echo $category[0]->cat_name; ?> 
  <?php echo get_the_category_list(); ?></span>
<a class="blog_post_title" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php the_title();
		//$like_count = get_post_meta( get_the_ID(), "_post_like_count", true );
		//echo '=>'. $like_count;
		?>
    </a>
<span class="blog_date"><?php echo $pfx_date = get_the_date( 'j F, Y', $post_id ); ?></span>

 <?php the_excerpt(); 
 //the_content();
 ?>


</div>
<?php endwhile; 
echo '<div class="pagination-blog">';

 echo paginate_links();
 echo '</div>';

// Reset Query
wp_reset_query();

if(isset($_POST['cat_id'])) exit;

}
//mail format
add_filter('wp_mail_from', 'new_mail_from');
add_filter('wp_mail_from_name', 'new_mail_from_name');

function new_mail_from($old) {
return 'thea@youngfoodies.co.uk';
}
function new_mail_from_name($old) {
echo '<p class="contact_message" style="color:red">'; echo 'Theadora Alexander'; echo '</p>';
}

add_action( 'init', 'my_prefix_bp_wp_mail_html_filters' );
function my_prefix_bp_wp_mail_html_filters() {
    add_filter( 'bp_email_use_wp_mail', function( $bool ) {
      return false;
    }, 10, 1 );
}

add_action('save_post', 'save_details');
function save_details()
{
	global $post;
	
	$memberships = array(1,2);
	foreach($memberships as $membership){
		delete_post_meta($post->ID, 'page_levels-'.$membership);
	}
	
	if(count($_POST["page_levels"]) > 0 && is_admin() && isset($_POST["page_levels"]) ){
		foreach($_POST["page_levels"] as $membship_level_local){
			$meta_key = 'page_levels-'.$membship_level_local;
			update_post_meta($post->ID, $meta_key, $membship_level_local);
		}
	}
}
add_filter( 'the_content', 'wpautop');