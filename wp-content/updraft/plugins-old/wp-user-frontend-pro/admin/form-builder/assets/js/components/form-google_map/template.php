<div class="wpuf-fields">
    <div :class="['wpuf-form-google-map-container', 'yes' === field.address ? 'show-search-box': 'hide-search-box']">
        <input class="wpuf-google-map-search" type="text" placeholder="<?php _e( 'Search address', 'wpuf-pro' ); ?>">
        <div class="wpuf-form-google-map"></div>
    </div>

    <span v-if="field.help" class="wpuf-help">{{ field.help }}</span>
</div>
