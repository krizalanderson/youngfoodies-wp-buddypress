<?php
/**
 * Plugin Name: WP FAQ Pro
 * Plugin URL: https://www.wponlinesupport.com
 * Description: A simple PRO FAQ plugin to display FAQ's on your website.
 * Text Domain: sp-faq
 * Domain Path: languages
 * Author: WP Online Support
 * Author URI: https://www.wponlinesupport.com
 * Contributors: WP Online Support
 * Version: 1.2.1
*/

if( !defined( 'WP_FAQP_VERSION' ) ) {
    define( 'WP_FAQP_VERSION', '1.2.1' ); // Version of plugin
}
if( !defined( 'WP_FAQP_DIR' ) ) {
    define( 'WP_FAQP_DIR', dirname( __FILE__ ) ); // Plugin dir
}
if( !defined( 'WP_FAQP_URL' ) ) {
    define( 'WP_FAQP_URL', plugin_dir_url( __FILE__ ) ); // Plugin url
}
if( !defined( 'WP_FAQP_BASENAME' ) ) {
    define( 'WP_FAQP_BASENAME', plugin_basename( __FILE__ ) ); // Plugin base name
}
if( !defined( 'WP_FAQP_POST_TYPE' ) ) {
    define( 'WP_FAQP_POST_TYPE', 'sp_faq' ); // Plugin post type
}
if( !defined( 'WP_FAQP_CAT' ) ) {
    define( 'WP_FAQP_CAT', 'faq_cat' ); // Plugin category name
}
if( !defined( 'WP_FAQP_META_PREFIX' ) ) {
    define( 'WP_FAQP_META_PREFIX', '_wp_faqp_' ); // Plugin category name
}

/**
 * Load Text Domain
 * This gets the plugin ready for translation
 * 
 * @package WP FAQ Pro
 * @since 1.0.0
 */
function wp_faqp_load_textdomain() {

    global $wp_version;

    // Set filter for plugin's languages directory
    $wp_faqp_lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
    $wp_faqp_lang_dir = apply_filters( 'wp_faqp_languages_directory', $wp_faqp_lang_dir );

    // Traditional WordPress plugin locale filter.
    $get_locale = get_locale();

    if ( $wp_version >= 4.7 ) {
        $get_locale = get_user_locale();
    }

    // Traditional WordPress plugin locale filter
    $locale = apply_filters( 'plugin_locale',  $get_locale, 'sp-faq' );
    $mofile = sprintf( '%1$s-%2$s.mo', 'sp-faq', $locale );

    // Setup paths to current locale file
    $mofile_global  = WP_LANG_DIR . '/plugins/' . basename( WP_FAQP_DIR ) . '/' . $mofile;

    if ( file_exists( $mofile_global ) ) { // Look in global /wp-content/languages/plugin-name folder
        load_textdomain( 'sp-faq', $mofile_global );
    } else { // Load the default language files
        load_plugin_textdomain( 'sp-faq', false, $wp_faqp_lang_dir );
    }
}
add_action('plugins_loaded', 'wp_faqp_load_textdomain');

/***** Updater Code Starts *****/
define( 'EDD_FAQ_STORE_URL', 'https://www.wponlinesupport.com' );
define( 'EDD_FAQ_ITEM_NAME', 'WP FAQ Pro' );

if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {    
    include( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
}

/**
 * Updater Function
 * 
 * @package WP FAQ Pro
 * @since 1.0.0
 */
function edd_sl_faq_plugin_updater() {
    
    $license_key = trim( get_option( 'edd_faq_license_key' ) );

    $edd_updater = new EDD_SL_Plugin_Updater( EDD_FAQ_STORE_URL, __FILE__, array(
            'version'   => WP_FAQP_VERSION,     // current version number
            'license'   => $license_key,        // license key (used get_option above to retrieve from DB)
            'item_name' => EDD_FAQ_ITEM_NAME,   // name of this plugin
            'author'    => 'WP Online Support'  // author of this plugin
          )
    );
}
add_action( 'admin_init', 'edd_sl_faq_plugin_updater', 0 );
include( dirname( __FILE__ ) . '/edd-faq-plugin.php' );
/***** Updater Code Ends *****/

/**
 * Activation Hook
 * 
 * Register plugin activation hook.
 * 
 * @package WP FAQ Pro
 * @since 1.0.0
 */
register_activation_hook( __FILE__, 'wp_faqp_install' );

/**
 * Deactivation Hook
 * Register plugin deactivation hook.
 * 
 * @package WP FAQ Pro
 * @since 1.0.0
 */
register_deactivation_hook( __FILE__, 'wp_faqp_uninstall');

/**
 * Plugin Setup (On Activation)
 * 
 * Does the initial setup,
 * stest default values for the plugin options.
 * 
 * @package WP FAQ Pro
 * @since 1.0.0
 */
function wp_faqp_install() {

    wp_faqp_register_post_type();
    wp_faqp_register_taxonomies();

    // IMP need to flush rules for custom registered post type
    flush_rewrite_rules();

    // Get settings for the plugin
    $wp_faqp_options = get_option( 'wp_faqp_options' );
    
    if( empty( $wp_faqp_options ) ) { // Check plugin version option
        
        // Set default settings
        wp_faqp_default_settings();
        
        // Update plugin version to option
        update_option( 'wp_faqp_plugin_version', '1.0' );
    }

    // Deactivate free version of plugin
    if( is_plugin_active('sp-faq/faq.php') ) {
        add_action('update_option_active_plugins', 'wp_faqp_deactivate_free_version');
    }
}

/**
 * Plugin Setup (On Deactivation)
 * 
 * Delete  plugin options.
 * 
 * @package WP FAQ Pro
 * @since 1.0.0
 */
function wp_faqp_uninstall() {
    // Uninstall functionality
}

/**
 * Deactivate free plugin
 * 
 * @package WP FAQ Pro
 * @since 1.0.0
 */
function wp_faqp_deactivate_free_version() {
    deactivate_plugins('sp-faq/faq.php', true);
}

/**
 * Function to display admin notice of activated plugin.
 * 
 * @package WP FAQ Pro
 * @since 1.0.0
 */
function wp_faqp_admin_notice() {
    
    $dir = WP_PLUGIN_DIR . '/sp-faq/faq.php';
    
    if( is_plugin_active( 'wp-faq-pro/faq.php' ) && file_exists($dir) ) {
        
        global $pagenow;

        if( $pagenow == 'plugins.php' && current_user_can('install_plugins') ) {
            echo '<div id="message" class="updated notice is-dismissible">
                    <p><strong>'.sprintf( __('Thank you for activating %s', 'sp-faq'), 'WP FAQ Pro' ).'</strong>.<br />
                    '.sprintf( __('It looks like you had FREE version %s of this plugin activated. To avoid conflicts the extra version has been deactivated and we recommend you delete it.', 'sp-faq'), '<strong>(<em>WP FAQ</em>)</strong>' ).'
                    </p>
                  </div>';
        }
    }
}

// Action to display notice
add_action( 'admin_notices', 'wp_faqp_admin_notice');

//Taking some globals
global $wp_faqp_options;

// Function File
require_once ( WP_FAQP_DIR . '/includes/wp-faqp-functions.php' );
$wp_faqp_options = wp_faqp_get_settings();

// Post Type File
require_once( WP_FAQP_DIR . '/includes/wp-faqp-post-types.php' );

// Script Class File
require_once ( WP_FAQP_DIR . '/includes/class-wp-faqp-script.php' );

// Admin Class File
require_once ( WP_FAQP_DIR . '/includes/admin/wp-faqp-class-admin.php' );

// Public Class File
require_once ( WP_FAQP_DIR . '/includes/class-wp-faqp-public.php' );

// Shortcode File
require_once( WP_FAQP_DIR . '/includes/shortcode/wp-faqp-faq.php' );
require_once( WP_FAQP_DIR . '/includes/shortcode/wp-faqp-faq-grid.php' );

// Templating functions
require_once( WP_FAQP_DIR . '/includes/wp-faqp-template-functions.php' );

// VC File for Shortcode
require_once( WP_FAQP_DIR . '/includes/admin/class-wp-faqp-vc.php' );

// Load admin side files
if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {   
    // Designs file
    include_once( WP_FAQP_DIR . '/includes/admin/wp-faqp-how-it-work.php' );
}