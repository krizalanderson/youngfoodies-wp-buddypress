=== WP FAQ Pro ===
Contributors: wponlinesupport, anoopranawat 
Tags: faq, faq list, faq plugin, faqs, jquery ui, wp-faq with category, jquery ui accordion,  faq with accordion, custom post type with accordion, frequently asked questions, wordpress, wordpress faq, WordPress Plugin, shortcodes
Requires at least: 3.1
Tested up to: 4.7.2
Author URI: http://wponlinesupport.com

A quick, easy way to add an responsive FAQs page. You can also use this plugin as a accordion.

== Description ==
Many CMS site needs a FAQs section. SP faqs pro plugin  allows you add, manage and display FAQ on your wordpress website.
This plugin is created with custom post type. Now you can also Fillter OR Display FAQ by category.


= Complete PRO Shrtcode is =
<code>[sp_faq limit="10"  category="category_ID" design="design-2" grid="2" category_name="sports"  single_open="false"
transition_speed="300" background_color="#000" font_color="#fff" border_color="#444" heading_font_size="20"]</code>

= Pro Shortcode Parameters are =

= Following are FAQ Parameters =

<code>[sp_faq]</code>

* **Limit** : [sp_faq limit="10"] (Limit the number FAQ's items to be display. By default value is limit="20".)
* **Category** : [sp_faq category="category_ID"] (Display FAQ's by category.)
* **Category Name** : [sp_faq category_name="category name"] (Display FAQ's category name. It will display above FAQ.)
* **Include Cat Child :** [sp_faq include_cat_child="true"] (Display child category FAQ. Values are "true" OR "false".)
* **Design** : [sp_faq design="design-1"] (Select design for faq. We have added 8 colors design ie design-1, design-2, design-3 to design-8.)
* **Single Open** : [sp_faq single_open="true"] (Display One FAQ item when click to open. By default value is "true". Values are "true" and "false".)
* **Default Open FAQ** : [sp_faq default_open_faq="2"] (Default open FAQ on page load.)
* **Transition Speed** : [sp_faq transition_speed="300"] (Transition speed when user click to open FAQ item.)
* **Background Color** : [sp_faq background_color="#000"] (Set background color of FAQ item.)
* **Font Color** : [sp_faq font_color="#fff"] (Set font color of FAQ item.)
* **Border Color** : [sp_faq border_color="#444"] (Set border color of FAQ box.)
* **Heading Font Size** : [sp_faq heading_font_size="20"] (Set font size for FAQ heading.)
* **Active FAQ Background Color** : [sp_faq active_bg_color="#fff"] (Set open FAQ background color.)
* **Active FAQ Font Color** : [sp_faq active_font_color="#000"] (Set open FAQ font color.)
* **Active FAQ Title Color** : [sp_faq active_title_color="#000"] (Set open FAQ title font color.)
* **Icon Color** : [sp_faq icon_color="white"] (Set the icon color. By default value is "black". Options are "black" OR white".)
* **Icon Type** : [sp_faq icon_type="plus"] (Set the icon type. By default value is "arrow". Options are "plus" OR arrtow".)
* **Icon Position** : [sp_faq icon_position="left"] (Set the icon position. By default value is "right". Options are "left" OR right".)
* **Order** : [sp_faq order="DESC"] (Set the FAQ order. Options are "DESC" OR ASC")
* **Orderby** : [sp_faq orderby="post_date"] (Set the FAQ orderby. Default value is 'post_date'. Options are "ID", "title", "post_date", "modified", "rand", "menu_order".)
* **Posts :** [sp_faq posts="1,5,6"] (Display only specific FAQ posts.)
* **Exclude Post :** [sp_faq exclude_post="1,5,6"] (Exclude some FAQ post which you do not want to display.)
* **Exclude Category :** [sp_faq exclude_cat="1,5,6"] (Exclude some FAQ category which you do not want to display.)


= Following are FAQ Category Grid Parameters =

<code>[faq-category-grid]</code>

* **Limit** : [faq-category-grid limit="10"] (Limit the number of FAQ's items to be display in a category block. By default value is limit="20")
* **Grid** : [faq-category-grid grid="2"] (Display FAQ's category in grid view.)
* **Border Color** : [faq-category-grid border_color="#444"] (Set border color of FAQ box.)
* **Background Color** : [faq-category-grid background_color="#000"] (ie Set background color of FAQ item )
* **Font Color** : [faq-category-grid font_color="#fff"] (ie Set font color of FAQ item )
* **Font Size** : [faq-category-grid font_size="20"] (Set font size for FAQ)
* **Order** : [faq-category-grid order="DESC"] (Set the FAQ post order. Options are "DESC" OR ASC")
* **Orderby** : [faq-category-grid orderby="post_date"] (Set the FAQ orderby. Default value is 'post_date'. Options are "ID", "title", "post_date", "modified", "rand", "menu_order")
* **Posts :** [faq-category-grid posts="1,5,6"] (Display only specific FAQ posts.)
* **Exclude Post :** [faq-category-grid exclude_post="1,5,6"] (Exclude some FAQ post which you do not want to display.)
* **Cat Orderby :** [faq-category-grid cat_orderby="name"] (Order FAQ category. Default value is 'name'. Options are 'name', 'slug', 'id' OR 'count')
* **Cat Order :** [faq-category-grid cat_order="ASC"] (Set the FAQ category order. Options are "DESC" OR ASC")
* **Cat Limit** : [faq-category-grid cat_limit="10"] (Limit the number of FAQ's category. Leave empty for all.)
* **Category** : [faq-category-grid cats="1,2,3"] (Display specific FAQ's category.)
* **Exclude Category :** [faq-category-grid exclude_cat="1,5,6"] (Exclude some FAQ category which you do not want to display.)
* **Display Post Count :** [faq-category-grid display_post_count="true"] (Display FAQ post count with category name. Options are 'true' or 'false'.)
* **Post to Show :** [faq-category-grid post_to_show="2"] (Display FAQ post at a time in category block, others will be toggle.)
* **Show More Button :** [faq-category-grid show_more_button="Show More"] (Change show more button text.)
* **Show Less Button :** [faq-category-grid show_less_button="Show Less"] (Change show less button text.)


**Added New Shortcode to display FAQ's items with categories in the grid view**
<code>[faq-category-grid grid="2" background_color="#f1f1f1" font_color="#000" heading_font_size="20"]</code>

Here is the example :
<code>
[sp_faq  category="category_ID" category_name="News"]
[sp_faq  category="category_ID" category_name="Sports"]
</code>

To use this FAQ plugin just create a new page and add this FAQ short code 
<code>[sp_faq]</code> 

OR

If you want to display FAQ by category then use this short code 
<code>[sp_faq  category="category_ID"]</code>

This faqs plugin add a FAQs page in your wordpress website with accordion.

= Features include: =
* wp-faq with category <code>[sp_faq  category="category_ID"]</code>
* Just create a FAQs page and add short code <code>[sp_faq]</code>
* Accordion with animation
* Added parameters
* Add thumb image for FAQ
* Easy to configure FAQ page
* Smooth FAQ Accordion effect
* Smoothly integrates this paq plugin into any theme
* CSS and JS file for FAQ custmization
* Search Engine Friendly URLs

== Installation ==

1. Upload the 'sp-faq-pro' folder to the '/wp-content/plugins/' directory.
2. Activate the sp-faq-pro list plugin through the 'Plugins' menu in WordPress.
3. Add a new page and add this short code <code>[sp_faq]</code>

== Changelog ==

= 1.2.1 (01, March 2017) =
* [+] Added 'active_font_color' shortcode parameter in [sp_faq] for open faq font color.
* [+] Added 'active_title_color' shortcode parameter in [sp_faq] for open faq title color.
* [*] Updated plugin translation code. Now user can put plugin languages file in WordPress 'language' folder so plugin language file will not be loss while plugin update.
* [*] Updated CSS for smooth effects.
* [*] Updated layout for 'faq-category-grid'.

= 1.2 (11, Nov 2016) =
* [+] Added 'How it Work' page for better user interface.
* [-] Removed 'Plugin Design' page.

= 1.1.9 (12, Sep 2016) =
* [*] Removed plugin license page from plugin section and added in 'FAQ Pro' menu.
* [*] Updated plugin license page.
* Added SSL to https://www.wponlinesupport.com/ for secure updates.

= 1.1.8 (25, July 2016) =
* [+] Added 'default_open_faq' shortcode parameter in [sp_faq] to remain open FAQ on page load.
* [+] Added 'include_cat_child' shortcode parameter in [sp_faq] to include child category FAQ or not.
* [*] Resolved Custom CSS display issue in head.
* [*] Updated some plugin CSS.
* [*] Updated FAQ js.

= 1.1.7 (20, June 2016) =
* [+] Added new designs.
* [*] Optimized Js for better performance.
* [*] Optimized code.
* [+] Added multiple FAQ shortcode support in a single page.
* [+] Added WooCommerce FAQ support. Now you can add Product FAQ to WooCommerce Product page easily.
* [+] Added support for Visual Composer. Now you can add shortcode from the Visual Composer.
* [+] Added plugin settings page for custom css and other settings.
* [+] Added 'posts' parameter to display specific faq post.
* [+] Added 'exclude_post' parameter to exclude some faq post.
* [+] Added 'exclude_cat' parameter to exclude some category post.
* [+] Added 'show_more_button' parameter for [faq-category-grid] shortcode to change show more button text.
* [+] Added 'show_less_button' parameter for [faq-category-grid] shortcode to change show less button text.
* [+] Added 'post_to_show' parameter for [faq-category-grid] shortcode to display faq at a time, others will be toggle.
* [+] Added 'display_post_count' parameter for [faq-category-grid] shortcode to display faq post count with name.
* [+] Added 'cat_limit' parameter for [faq-category-grid] shortcode to limit faq category.
* [+] Added 'cat_order' parameter for [faq-category-grid] shortcode to sort faq category.
* [+] Added 'cat_orderby' parameter for [faq-category-grid] shortcode to order faq category. For detail shortcode parameter please check plugin documentation.
* [-] Removed grid parameter from the [sp_faq] shortcode.

= 1.1.6 =
* Fixed some css issues.

= 1.1.5 =
* Added 'order' and 'orderby' parameter in shortcode.
* Added 'Drag & Drop' interface for FAQ's ordering.
* Optimize some code.

= 1.1.4 =
* Fixed plugin update error.

= 1.1.3 =
* Fixed some css issues.
* Resolved multiple FAQ jQuery conflict issue.

= 1.1.2 =
* Fixed some bug

= 1.1.1 =
* Fixed some bug
* Added New shortcode <code>[faq-category-grid]</code>

= 1.1 =
* Fixed some bug.

= 1.0 =
* Initial release.


== Upgrade Notice ==

= 1.2.1 (01, March 2017) =
* [+] Added 'active_font_color' shortcode parameter in [sp_faq] for open faq font color.
* [+] Added 'active_title_color' shortcode parameter in [sp_faq] for open faq title color.
* [*] Updated plugin translation code. Now user can put plugin languages file in WordPress 'language' folder so plugin language file will not be loss while plugin update.
* [*] Updated CSS for smooth effects.
* [*] Updated layout for 'faq-category-grid'.

= 1.2 (10, Nov 2016) =
* [+] Added 'How it Work' page for better user interface.
* [-] Removed 'Plugin Design' page.

= 1.1.9 (12, Sep 2016) =
* [*] Removed plugin license page from plugin section and added in 'FAQ Pro' menu.
* [*] Updated plugin license page.
* Added SSL to https://www.wponlinesupport.com/ for secure updates.

= 1.1.8 (25, July 2016) =
* [+] Added 'default_open_faq' shortcode parameter in [sp_faq] to remain open FAQ on page load.
* [+] Added 'include_cat_child' shortcode parameter in [sp_faq] to include child category FAQ or not.
* [*] Resolved Custom CSS display issue in head.
* [*] Updated some plugin CSS.
* [*] Updated FAQ js.

= 1.1.7 (20, June 2016) =
* [+] Added new designs.
* [*] Optimized Js for better performance.
* [*] Optimized code.
* [+] Added multiple FAQ shortcode support in a single page.
* [+] Added WooCommerce FAQ support. Now you can add Product FAQ to WooCommerce Product page easily.
* [+] Added support for Visual Composer. Now you can add shortcode from the Visual Composer.
* [+] Added plugin settings page for custom css and other settings.
* [+] Added 'posts' parameter to display specific faq post.
* [+] Added 'exclude_post' parameter to exclude some faq post.
* [+] Added 'exclude_cat' parameter to exclude some category post.
* [+] Added 'show_more_button' parameter for [faq-category-grid] shortcode to change show more button text.
* [+] Added 'show_less_button' parameter for [faq-category-grid] shortcode to change show less button text.
* [+] Added 'post_to_show' parameter for [faq-category-grid] shortcode to display faq at a time, others will be toggle.
* [+] Added 'display_post_count' parameter for [faq-category-grid] shortcode to display faq post count with name.
* [+] Added 'cat_limit' parameter for [faq-category-grid] shortcode to limit faq category.
* [+] Added 'cat_order' parameter for [faq-category-grid] shortcode to sort faq category.
* [+] Added 'cat_orderby' parameter for [faq-category-grid] shortcode to order faq category. For detail shortcode parameter please check plugin documentation.
* [-] Removed grid parameter from the [sp_faq] shortcode.

= 1.1.6 =
* Fixed some css issues.

= 1.1.5 =
* Added 'order' and 'orderby' parameter in shortcode.
* Added 'Drag & Drop' interface for FAQ's ordering.
* Optimize some code.

= 1.1.4 =
* Fixed plugin update error.

= 1.1.3 =
* Fixed some css issues.
* Resolved multiple FAQ jQuery conflict issue.

= 1.1.2 =
* Fixed some bug

= 1.1.1 =
* Fixed some bug
* Added New shortcode <code>[faq-category-grid]</code>

= 1.1 =
* Fixed some bug.

= 1.0 =
* Initial release.