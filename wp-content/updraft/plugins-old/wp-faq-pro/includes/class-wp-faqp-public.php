<?php
/**
 * Public Class
 *
 * Handles the public side functionality of plugin
 *
 * @package WP FAQ Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class Wp_Faqp_Public {
	
	function __construct() {
		
		$enable_woo_faq = wp_faqp_get_option('enable_woo_faq');
		
		if( !empty($enable_woo_faq) ) {
			
			// WooCommerce Product Tabs
			add_filter( 'woocommerce_product_tabs', array($this, 'wp_faqp_woo_product_tab') );
		}
	}

	/**
	 * Add `FAQ` tab to Woocommerce Product Page
	 * 
	 * @package WP FAQ Pro
	 * @since 1.1.7
	 */
	function wp_faqp_woo_product_tab( $tabs ) {
		
		global $post;
		
		// Taking some variables
		$prefix 		= WP_FAQP_META_PREFIX;
		$wp_faqp_enable = get_post_meta( $post->ID, $prefix.'enable', true );
		
		if( !empty($wp_faqp_enable) ) {
			$tabs['wp_faqp'] = array(
									'title' 	=> wp_faqp_get_option('woo_faq_tab_text'),
									'priority' 	=> 26,
									'callback' 	=> 'wp_faqp_product_faq_tab'
								);
		}
		
		return $tabs;
	}
}

$wp_faqp_public = new Wp_Faqp_Public();