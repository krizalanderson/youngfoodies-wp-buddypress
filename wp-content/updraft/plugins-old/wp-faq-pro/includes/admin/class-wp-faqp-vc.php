<?php
/**
 * Visual Composer Class
 *
 * Handles the visual composer shortcode functionality of plugin
 *
 * @package WP FAQ Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class Wp_Faqp_Vc {
	
	function __construct() {

		// Action to add 'featured-cnt-img' shortcode in vc
		add_action( 'vc_before_init', array($this, 'wp_faqp_integrate_sp_faq_vc') );

		// Action to add 'featured-cnt-img' shortcode in vc
		add_action( 'vc_before_init', array($this, 'wp_faqp_integrate_faq_category_grid_vc') );
	}
	
	/**
	 * Function to add 'featured-cnt-img' shortcode in vc
	 * 
	 * @package WP FAQ Pro
	 * @since 1.0.0
	 */
	function wp_faqp_integrate_sp_faq_vc() {
		vc_map( array(
			'name' 			=> __( 'WPOS - FAQ', 'sp-faq' ),
			'base' 			=> 'sp_faq',
			'icon' 			=> 'icon-wpb-wp',
			'class' 		=> '',
			'category' 		=> 'Content',
			'description' 	=> __( 'Display FAQ.', 'sp-faq' ),
			'params' 	=> array(
								// General settings
								array(
									'type' 			=> 'dropdown',
									'class' 		=> '',
									'heading' 		=> __( 'Design', 'sp-faq' ),
									'param_name' 	=> 'design',
									'value' 		=> array(
															__( 'Design 1', 'sp-faq' ) 		=> 'design-1',
															__( 'Design 2', 'sp-faq' ) 		=> 'design-2',
															__( 'Design 3', 'sp-faq' ) 		=> 'design-3',
															__( 'Design 4', 'sp-faq' ) 		=> 'design-4',
															__( 'Design 5', 'sp-faq' ) 		=> 'design-5',
															__( 'Design 6', 'sp-faq' ) 		=> 'design-6',
															__( 'Design 7', 'sp-faq' ) 		=> 'design-7',
															__( 'Design 8', 'sp-faq' ) 		=> 'design-8',
															__( 'Design 9', 'sp-faq' ) 		=> 'design-9',
															__( 'Design 10', 'sp-faq' ) 	=> 'design-10',
															__( 'Design 11', 'sp-faq' ) 	=> 'design-11',
															__( 'Design 12', 'sp-faq' ) 	=> 'design-12',
															__( 'Design 13', 'sp-faq' ) 	=> 'design-13',
															__( 'Design 14', 'sp-faq' ) 	=> 'design-14',
															__( 'Design 15', 'sp-faq' ) 	=> 'design-15',
														),
									'description' 	=> __( 'Select predefined color theme for FAQ.', 'sp-faq' ),
									'admin_label' 	=> true,
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'FAQ Heading', 'sp-faq' ),
									'param_name' 	=> 'category_name',
									'value' 		=> '',
									'description' 	=> __( 'Enter FAQ heading title. A simple title which will be displayed above faq.', 'sp-faq' ),
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Default Open FAQ Number', 'sp-faq' ),
									'param_name' 	=> 'default_open_faq',
									'value' 		=> '',
									'description' 	=> __( 'Enter the number of FAQ which will be remian open default on page load.', 'sp-faq' ),
								),
								array(
									'type' 			=> 'dropdown',
									'class' 		=> '',
									'heading' 		=> __( 'Single Open FAQ', 'sp-faq' ),
									'param_name' 	=> 'single_open',
									'value' 		=> array(
														__( 'True', 'sp-faq' ) 	=> 'true',
														__( 'False', 'sp-faq' ) 	=> 'false',
													),
									'description' 	=> __( 'Remain open one FAQ at a time.', 'sp-faq' ),
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Transition Speed', 'sp-faq' ),
									'param_name' 	=> 'transition_speed',
									'value' 		=> 300,
									'description' 	=> __( 'Enter FAQ transition speed.', 'sp-faq' ),
								),

								// Design Options
								array(
									'type' 			=> 'colorpicker',
									'class' 		=> '',
									'heading' 		=> __( 'Background Color', 'sp-faq' ),
									'param_name' 	=> 'background_color',
									'value' 		=> '',
									'description' 	=> __( 'Choose faq background color.', 'sp-faq' ),
									'group' 		=> __( 'Design Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'colorpicker',
									'class' 		=> '',
									'heading' 		=> __( 'Active FAQ Background Color', 'sp-faq' ),
									'param_name' 	=> 'active_bg_color',
									'value' 		=> '',
									'description' 	=> __( 'Choose open faq background color.', 'sp-faq' ),
									'group' 		=> __( 'Design Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'colorpicker',
									'class' 		=> '',
									'heading' 		=> __( 'Font Color', 'sp-faq' ),
									'param_name' 	=> 'font_color',
									'value' 		=> '',
									'description' 	=> __( 'Choose faq font color.', 'sp-faq' ),
									'group' 		=> __( 'Design Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'colorpicker',
									'class' 		=> '',
									'heading' 		=> __( 'Active FAQ Font Color', 'sp-faq' ),
									'param_name' 	=> 'active_font_color',
									'value' 		=> '',
									'description' 	=> __( 'Choose open faq font color.', 'sp-faq' ),
									'group' 		=> __( 'Design Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'colorpicker',
									'class' 		=> '',
									'heading' 		=> __( 'Active FAQ Title Font Color', 'sp-faq' ),
									'param_name' 	=> 'active_title_color',
									'value' 		=> '',
									'description' 	=> __( 'Choose open faq title font color.', 'sp-faq' ),
									'group' 		=> __( 'Design Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'colorpicker',
									'class' 		=> '',
									'heading' 		=> __( 'Border Color', 'sp-faq' ),
									'param_name' 	=> 'border_color',
									'value' 		=> '',
									'description' 	=> __( 'Choose faq border color.', 'sp-faq' ),
									'group' 		=> __( 'Design Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Heading Font Size', 'sp-faq' ),
									'param_name' 	=> 'heading_font_size',
									'value' 		=> 15,
									'description' 	=> __( 'Enter heading font color.', 'sp-faq' ),
									'group' 		=> __( 'Design Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'dropdown',
									'class' 		=> '',
									'heading' 		=> __( 'Icon Color', 'sp-faq' ),
									'param_name' 	=> 'icon_color',
									'value' 		=> array(
														__( 'Black', 'sp-faq' ) 	=> 'black',
														__( 'White', 'sp-faq' ) 	=> 'white',
													),
									'description' 	=> __( 'Choose icon color.', 'sp-faq' ),
									'group' 		=> __( 'Design Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'dropdown',
									'class' 		=> '',
									'heading' 		=> __( 'Icon Type', 'sp-faq' ),
									'param_name' 	=> 'icon_type',
									'value' 		=> array(
														__( 'Arrow', 'sp-faq' ) => 'arrow',
														__( 'Plus', 'sp-faq' ) 	=> 'plus',
													),
									'description' 	=> __( 'Choose icon type.', 'sp-faq' ),
									'group' 		=> __( 'Design Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'dropdown',
									'class' 		=> '',
									'heading' 		=> __( 'Icon Position', 'sp-faq' ),
									'param_name' 	=> 'icon_position',
									'value' 		=> array(
														__( 'Right', 'sp-faq' ) => 'right',
														__( 'Left', 'sp-faq' ) 	=> 'left',
													),
									'description' 	=> __( 'Choose icon position.', 'sp-faq' ),
									'group' 		=> __( 'Design Settings', 'sp-faq' ),
								),

								// Data Settings
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Total items', 'sp-faq' ),
									'param_name' 	=> 'limit',
									'value' 		=> 15,
									'description' 	=> __( 'Enter number of faq post to be displayed. Enter -1 to display all.', 'sp-faq' ),
									'group' 		=> __( 'Data Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'dropdown',
									'class' 		=> '',
									'heading' 		=> __( 'Order By', 'sp-faq' ),
									'param_name' 	=> 'orderby',
									'value' 		=> array(
														__( 'Post Date', 'sp-faq' ) 		=> 'date',
														__( 'Post ID', 'sp-faq' ) 			=> 'ID',
														__( 'Post Author', 'sp-faq' ) 		=> 'author',
														__( 'Post Title', 'sp-faq' ) 		=> 'title',
														__( 'Post Slug', 'sp-faq' )	 		=> 'name',
														__( 'Post Modified Date', 'sp-faq' ) => 'modified',
														__( 'Random', 'sp-faq' ) 			=> 'rand',
														__( 'Menu Order', 'sp-faq' ) 		=> 'menu_order',
													),
									'description' 	=> __( 'Select order type.', 'sp-faq' ),
									'group' 		=> __( 'Data Settings', 'sp-faq' )
								),
								array(
									'type' 			=> 'dropdown',
									'class' 		=> '',
									'heading' 		=> __( 'Sort order', 'sp-faq' ),
									'param_name' 	=> 'order',
									'value' 		=> array(
														__( 'Descending', 'sp-faq' ) 	=> 'desc',
														__( 'Ascending', 'sp-faq' ) 	=> 'asc',
													),
									'description' 	=> __( 'Select sorting order.', 'sp-faq' ),
									'group' 		=> __( 'Data Settings', 'sp-faq' )
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Display Specific Faq', 'sp-faq' ),
									'param_name' 	=> 'posts',
									'value' 		=> '',
									'description' 	=> sprintf(__( 'Enter faq post id which you want to display. You can pass multiple ids with comma seperated. You can find id at listing <a href="%1$s" target="_blank">page</a>.', 'sp-faq'), add_query_arg(array('post_type' => WP_FAQP_POST_TYPE), 'edit.php')),
									'group' 		=> __( 'Data Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Exclude Faq', 'sp-faq' ),
									'param_name' 	=> 'exclude_post',
									'value' 		=> '',
									'description' 	=> sprintf(__( 'Enter faq post id which you do not want to display. You can pass multiple ids with comma seperated. You can find id at listing <a href="%1$s" target="_blank">page</a>.', 'sp-faq'), add_query_arg(array('post_type' => WP_FAQP_POST_TYPE), 'edit.php')),
									'group' 		=> __( 'Data Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Category', 'sp-faq' ),
									'param_name' 	=> 'category',
									'value' 		=> '',
									'description' 	=> sprintf( __('Enter faq category id to display faq categories wise. You can pass multiple ids with comma seperated. You can find id at listing <a href="%1$s" target="_blank">page</a>.', 'sp-faq'), add_query_arg( array('taxonomy' => WP_FAQP_CAT, 'post_type' => WP_FAQP_POST_TYPE ), 'edit-tags.php')),
									'group' 		=> __( 'Data Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Exclude Category', 'sp-faq' ),
									'param_name' 	=> 'exclude_cat',
									'value' 		=> '',
									'description' 	=> sprintf(__( 'Exclude faq post category. You can pass multiple ids with comma seperated. Works only if `Category` field is empty. You can find id at listing <a href="%1$s" target="_blank">page</a>.', 'sp-faq'), add_query_arg( array('taxonomy' => WP_FAQP_CAT, 'post_type' => WP_FAQP_POST_TYPE ), 'edit-tags.php')),
									'group' 		=> __( 'Data Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'dropdown',
									'class' 		=> '',
									'heading' 		=> __( 'Include Category Children', 'sp-faq' ),
									'param_name' 	=> 'include_cat_child',
									'value' 		=> array(
														__( 'True', 'sp-faq' ) 	=> 'true',
														__( 'False', 'sp-faq' ) 	=> 'false',
													),
									'description' 	=> __( 'Include category children or not. If you choose parent category then whether to display child category FAQ.', 'sp-faq' ),
									'group' 		=> __( 'Data Settings', 'sp-faq' ),
								),			
							)
		));
	}

	/**
	 * Function to add 'featured-cnt-img' shortcode in vc
	 * 
	 * @package WP FAQ Pro
	 * @since 1.0.0
	 */
	function wp_faqp_integrate_faq_category_grid_vc() {
		vc_map( array(
			'name' 			=> __( 'WPOS - FAQ Cat Grid', 'sp-faq' ),
			'base' 			=> 'faq-category-grid',
			'icon' 			=> 'icon-wpb-wp',
			'class' 		=> '',
			'category' 		=> 'Content',
			'description' 	=> __( 'Display FAQ with category grid.', 'sp-faq' ),
			'params' 	=> array(

								// General settings
								array(
									'type' 			=> 'dropdown',
									'class' 		=> '',
									'heading' 		=> __( 'Grid Elements Per Row', 'sp-faq' ),
									'param_name' 	=> 'grid',
									'value' 		=> array(
															__( 'One Column', 'sp-faq' ) 	=> '1',
															__( 'Two Column', 'sp-faq' ) 	=> '2',
															__( 'Third Column', 'sp-faq' ) 	=> '3',
															__( 'Four Column', 'sp-faq' ) 	=> '4',
														),
									'std'			=> '1',
									'description' 	=> __( 'Select number of grid elements per row.', 'sp-faq' ),
									'admin_label' 	=> true,
								),
								array(
									'type' 			=> 'dropdown',
									'class' 		=> '',
									'heading' 		=> __( 'Display Post Count', 'sp-faq' ),
									'param_name' 	=> 'display_post_count',
									'value' 		=> array(
														__( 'True', 'sp-faq' ) 		=> 'true',
														__( 'False', 'sp-faq' ) 	=> 'false',
													),
									'std'			=> 'false',
									'description' 	=> __( 'Select sorting order.', 'sp-faq' ),
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'No of FAQ to Show', 'sp-faq' ),
									'param_name' 	=> 'post_to_show',
									'value' 		=> 2,
									'description' 	=> __( 'Enter number of FAQ to show, others will be hide for toggle.', 'sp-faq' ),
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Show More Button Text', 'sp-faq' ),
									'param_name' 	=> 'show_more_button',
									'value' 		=> __('Show More', 'sp-faq'),
									'description' 	=> __( 'Enter `Show More` button text.', 'sp-faq' ),
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Show Less Button Text', 'sp-faq' ),
									'param_name' 	=> 'show_less_button',
									'value' 		=> __('Show Less', 'sp-faq'),
									'description' 	=> __( 'Enter `Show Less` button text.', 'sp-faq' ),
								),

								// Design Options
								array(
									'type' 			=> 'colorpicker',
									'class' 		=> '',
									'heading' 		=> __( 'Background Color', 'sp-faq' ),
									'param_name' 	=> 'background_color',
									'value' 		=> '',
									'description' 	=> __( 'Choose faq background color.', 'sp-faq' ),
									'group' 		=> __( 'Design Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'colorpicker',
									'class' 		=> '',
									'heading' 		=> __( 'Font Color', 'sp-faq' ),
									'param_name' 	=> 'font_color',
									'value' 		=> '',
									'description' 	=> __( 'Choose faq font color.', 'sp-faq' ),
									'group' 		=> __( 'Design Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'colorpicker',
									'class' 		=> '',
									'heading' 		=> __( 'Border Color', 'sp-faq' ),
									'param_name' 	=> 'border_color',
									'value' 		=> '',
									'description' 	=> __( 'Choose faq border color.', 'sp-faq' ),
									'group' 		=> __( 'Design Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Font Size', 'sp-faq' ),
									'param_name' 	=> 'font_size',
									'value' 		=> 15,
									'description' 	=> __( 'Enter heading font color.', 'sp-faq' ),
									'group' 		=> __( 'Design Settings', 'sp-faq' ),
								),

								// Data Settings
								array(
									'type' 			=> 'dropdown',
									'class' 		=> '',
									'heading' 		=> __( 'Category Order By', 'sp-faq' ),
									'param_name' 	=> 'cat_orderby',
									'value' 		=> array(
														__( 'Name', 'sp-faq' ) 			=> 'name',
														__( 'Category Slug', 'sp-faq' ) => 'slug',
														__( 'Category ID', 'sp-faq' ) 	=> 'id',
														__( 'Post Count', 'sp-faq' ) 	=> 'count',
													),
									'description' 	=> __( 'Select order type for faq category.', 'sp-faq' ),
									'group' 		=> __( 'Data Settings', 'sp-faq' )
								),
								array(
									'type' 			=> 'dropdown',
									'class' 		=> '',
									'heading' 		=> __( 'Category Sort order', 'sp-faq' ),
									'param_name' 	=> 'cat_order',
									'value' 		=> array(
														__( 'Ascending', 'sp-faq' ) 	=> 'asc',
														__( 'Descending', 'sp-faq' ) 	=> 'desc',
													),
									'description' 	=> __( 'Select sorting order for category.', 'sp-faq' ),
									'group' 		=> __( 'Data Settings', 'sp-faq' )
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Category Limit', 'sp-faq' ),
									'param_name' 	=> 'cat_limit',
									'value' 		=> '',
									'description' 	=> __( 'Enter number of categories to show.', 'sp-faq' ),
									'group' 		=> __( 'Data Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Display Specific Category', 'sp-faq' ),
									'param_name' 	=> 'cats',
									'value' 		=> '',
									'description' 	=> sprintf( __('Display only specific category. You can pass multiple ids with comma seperated. You can find id at listing <a href="%1$s" target="_blank">page</a>.', 'sp-faq'), add_query_arg( array('taxonomy' => WP_FAQP_CAT, 'post_type' => WP_FAQP_POST_TYPE ), 'edit-tags.php')),
									'group' 		=> __( 'Data Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Exclude Category', 'sp-faq' ),
									'param_name' 	=> 'exclude_cat',
									'value' 		=> '',
									'description' 	=> sprintf( __('Exclude faq category. Works only if `Category` field is empty. You can pass multiple ids with comma seperated. You can find id at listing <a href="%1$s" target="_blank">page</a>.', 'sp-faq'), add_query_arg( array('taxonomy' => WP_FAQP_CAT, 'post_type' => WP_FAQP_POST_TYPE ), 'edit-tags.php')),
									'group' 		=> __( 'Data Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Faq Total Items', 'sp-faq' ),
									'param_name' 	=> 'limit',
									'value' 		=> 15,
									'description' 	=> __( 'Enter number of faq post to be displayed. Enter -1 to display all.', 'sp-faq' ),
									'group' 		=> __( 'Data Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'dropdown',
									'class' 		=> '',
									'heading' 		=> __( 'Faq Order By', 'sp-faq' ),
									'param_name' 	=> 'orderby',
									'value' 		=> array(
														__( 'Post Date', 'sp-faq' ) 		=> 'date',
														__( 'Post ID', 'sp-faq' ) 			=> 'ID',
														__( 'Post Author', 'sp-faq' ) 		=> 'author',
														__( 'Post Title', 'sp-faq' ) 		=> 'title',
														__( 'Post Slug', 'sp-faq' )	 		=> 'name',
														__( 'Post Modified Date', 'sp-faq' ) => 'modified',
														__( 'Random', 'sp-faq' ) 			=> 'rand',
														__( 'Menu Order', 'sp-faq' ) 		=> 'menu_order',
													),
									'description' 	=> __( 'Select order type for faq post.', 'sp-faq' ),
									'group' 		=> __( 'Data Settings', 'sp-faq' )
								),
								array(
									'type' 			=> 'dropdown',
									'class' 		=> '',
									'heading' 		=> __( 'Faq Sort Order', 'sp-faq' ),
									'param_name' 	=> 'order',
									'value' 		=> array(
														__( 'Descending', 'sp-faq' ) 	=> 'desc',
														__( 'Ascending', 'sp-faq' ) 	=> 'asc',
													),
									'description' 	=> __( 'Select sorting order for post.', 'sp-faq' ),
									'group' 		=> __( 'Data Settings', 'sp-faq' )
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Display Specific Faq', 'sp-faq' ),
									'param_name' 	=> 'posts',
									'value' 		=> '',
									'description' 	=> sprintf(__( 'Enter faq post id which you want to display. You can pass multiple ids with comma seperated. You can find id at listing <a href="%1$s" target="_blank">page</a>.', 'sp-faq'), add_query_arg(array('post_type' => WP_FAQP_POST_TYPE), 'edit.php')),
									'group' 		=> __( 'Data Settings', 'sp-faq' ),
								),
								array(
									'type' 			=> 'textfield',
									'class' 		=> '',
									'heading' 		=> __( 'Exclude Faq', 'sp-faq' ),
									'param_name' 	=> 'exclude_post',
									'value' 		=> '',
									'description' 	=> sprintf(__( 'Enter faq post id which you do not want to display. You can pass multiple ids with comma seperated. You can find id at listing <a href="%1$s" target="_blank">page</a>.', 'sp-faq'), add_query_arg(array('post_type' => WP_FAQP_POST_TYPE), 'edit.php')),
									'group' 		=> __( 'Data Settings', 'sp-faq' ),
								),
							)
			));
	}
}

$wp_faqp_vc = new Wp_Faqp_Vc();