<?php
/**
 * Admin Class
 *
 * Handles the Admin side functionality of plugin
 *
 * @package WP FAQ Pro
 * @since 1.1.6
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class Wp_Faqp_Admin {
	
	function __construct() {

		// Action to register plugin settings
		add_action ( 'admin_init', array($this,'wp_faqp_register_settings') );

		// Action to register admin menu
		add_action( 'admin_menu', array($this, 'wp_faqp_register_menu'), 9 );

		// Manage Category Shortcode Columns
		add_filter( 'manage_edit-'.WP_FAQP_CAT.'_columns', array($this, 'wp_faqp_cat_manage_columns') );
		add_action( 'manage_'.WP_FAQP_CAT.'_custom_column', array($this, 'wp_faqp_cat_columns'), 10, 3 );

		// Action to add sorting link at FAQ listing page
		add_filter( 'views_edit-'.WP_FAQP_POST_TYPE, array($this, 'wp_faqp_sorting_link') );

		// Action to add custom column to faq listing
		add_filter( 'manage_'.WP_FAQP_POST_TYPE.'_posts_columns', array($this, 'wp_faqp_posts_columns') );

		// Action to add custom column data to faq listing
		add_action('manage_'.WP_FAQP_POST_TYPE.'_posts_custom_column', array($this, 'wp_faqp_post_columns_data'), 10, 2);

		// Action to add category dropdown
		add_action( 'restrict_manage_posts', array($this, 'wp_faqp_add_post_filters'), 50 );

		// Filter to add row data
		add_filter( 'post_row_actions', array($this, 'wp_faqp_add_post_row_data'), 10, 2 );

		// Action to add 'Save Sort Order' button
		add_action( 'restrict_manage_posts', array($this, 'wp_faqp_restrict_manage_posts') );

		// Ajax call to update option
		add_action( 'wp_ajax_wp_faqp_update_post_order', array($this, 'wp_faqp_update_post_order'));
		add_action( 'wp_ajax_nopriv_wp_faqp_update_post_order',array( $this, 'wp_faqp_update_post_order'));

		// Filter to add plugin links
		add_filter( 'plugin_row_meta', array( $this, 'wp_faqp_plugin_row_meta' ), 10, 2 );

		// If WooCommerce FAQ is enable
		$enable_woo_faq = wp_faqp_get_option( 'enable_woo_faq' );
		if( !empty($enable_woo_faq) ) {
			
			// WooCommerce Product Data Tabs - Admin
			add_filter( 'woocommerce_product_data_tabs', array($this, 'wp_faqp_woo_product_data_tabs') );
			
			// WooCommerce Product Tabs Data - Admin
			add_action( 'woocommerce_product_data_panels', 'wp_faqp_woo_product_data_panels' );
			
			// Action to save metabox
			add_action( 'save_post', array($this,'wp_faqp_save_woo_metabox_value') );
		}
	}

	/**
	 * Function register setings
	 * 
	 * @package WP FAQ Pro
	 * @since 1.1.7
	 */
	function wp_faqp_register_settings() {
		register_setting( 'wp_faqp_plugin_options', 'wp_faqp_options', array($this, 'wp_faqp_validate_options') );
	}

	/**
	 * Validate Settings Options
	 * 
	 * @package WP FAQ Pro
	 * @since 1.1.7
	 */
	function wp_faqp_validate_options( $input ) {
		
		$input['enable_woo_faq']	= isset($input['enable_woo_faq']) 		? 1 : 0;
		$input['woo_faq_tab_text']	= !empty($input['woo_faq_tab_text']) 	? wp_faqp_slashes_deep($input['woo_faq_tab_text']) : __('FAQ', 'sp-faq');
		$input['custom_css'] 		= isset($input['custom_css']) 			? wp_faqp_slashes_deep($input['custom_css'], true) : '';
		
		return $input;
	}
	
	/**
	 * Function to register admin menus
	 * 
	 * @package WP FAQ Pro
	 * @since 1.1.7
	 */
	function wp_faqp_register_menu() {
		add_submenu_page( 'edit.php?post_type='.WP_FAQP_POST_TYPE, __('Settings', 'sp-faq'), __('Settings', 'sp-faq'), 'manage_options', 'wp-faqp-settings', array($this, 'wp_faqp_settings_page') );
	}

	/**
	 * Function to handle the setting page html
	 * 
	 * @package WP FAQ Pro
	 * @since 1.1.7
	 */
	function wp_faqp_settings_page() {
		include_once( WP_FAQP_DIR . '/includes/admin/settings/wp-faqp-settings.php' );
	}

	/**
	 * Add 'Sort FAQ' link at FAQ listing page
	 * 
	 * @package WP FAQ Pro
	 * @since 1.0.0
	 */
	function wp_faqp_cat_manage_columns($columns) {

		$new_columns['faq_category_shortcode'] = __( 'FAQ Category Shortcode', 'sp-faq' );

		$columns = wp_faqp_add_array( $columns, $new_columns, 2 );

		return $columns;
	}

	/**
	 * Add 'Sort FAQ' link at FAQ listing page
	 * 
	 * @package WP FAQ Pro
	 * @since 1.0.0
	 */
	function wp_faqp_cat_columns($out, $column_name, $theme_id) {
		
		switch ($column_name) {
			case 'faq_category_shortcode':             
			echo '[sp_faq category="' . $theme_id. '"]<br/>';
			echo '[faq-grid category="' . $theme_id. '"]';
			break;
			
			default:
			break;
		}  
	}

	/**
	 * Add 'Sort FAQ' link at FAQ listing page
	 * 
	 * @package WP FAQ Pro
	 * @since 1.1.5
	 */
	function wp_faqp_sorting_link( $views ) {
		
		global $post_type, $wp_query;

		$class            = ( isset( $wp_query->query['orderby'] ) && $wp_query->query['orderby'] == 'menu_order title' ) ? 'current' : '';
		$query_string     = remove_query_arg(array( 'orderby', 'order' ));
		$query_string     = add_query_arg( 'orderby', urlencode('menu_order title'), $query_string );
		$query_string     = add_query_arg( 'order', urlencode('ASC'), $query_string );
		$views['byorder'] = '<a href="' . esc_url( $query_string ) . '" class="' . esc_attr( $class ) . '">' . __( 'Sort FAQs', 'sp-faq' ) . '</a>';

		return $views;
	}

	/**
	 * Add custom column to FAQ listing page
	 * 
	 * @package WP FAQ Pro
	 * @since 1.1.5
	 */
	function wp_faqp_posts_columns( $columns ){

		$new_columns['sp_faq_order'] = __('Order', 'sp-faq');

		$columns = wp_faqp_add_array( $columns, $new_columns, 3 );

		return $columns;
	}

	/**
	 * Add custom column data to FAQ listing page
	 * 
	 * @package WP FAQ Pro
	 * @since 1.1.5
	 */
	function wp_faqp_post_columns_data( $column, $data ) {

		global $post;

		if( $column == 'sp_faq_order' ){
			$post_id            = isset($post->ID) ? $post->ID : '';
			$post_menu_order    = isset($post->menu_order) ? $post->menu_order : '';
			
			echo $post_menu_order;
			echo "<input type='hidden' value='{$post_id}' name='sp_faq_post[]' class='sp-faq-order' id='sp-faq-order-{$post_id}' />";
		}
	}

	/**
	 * Add category dropdown to FAQ listing page
	 * 
	 * @package WP FAQ Pro
	 * @since 1.1.7
	 */
	function wp_faqp_add_post_filters() {
		
		global $typenow;
		
		if( $typenow == WP_FAQP_POST_TYPE ) {

			$wp_faqp_cat = isset($_GET[WP_FAQP_CAT]) ? $_GET[WP_FAQP_CAT] : '';

			$dropdown_options = apply_filters('wp_faqp_cat_filter_args', array(
					'show_option_none' 	=> __('All Categories', 'sp-faq'),
					'option_none_value' => '',
					'hide_empty' 		=> 1,
					'hierarchical' 		=> 1,
					'show_count' 		=> 0,
					'orderby' 			=> 'name',
					'name'				=> WP_FAQP_CAT,
					'taxonomy'			=> WP_FAQP_CAT,
					'selected' 			=> $wp_faqp_cat,
					'value_field'		=> 'slug',
				));

			wp_dropdown_categories( $dropdown_options );
		}
	}

	/**
	 * Function to add custom quick links at post listing page
	 * 
	 * @package WP FAQ Pro
	 * @since 1.2.1
	 */
	function wp_faqp_add_post_row_data( $actions, $post ) {
		
		if( $post->post_type == WP_FAQP_POST_TYPE ) {
			return array_merge( array( 'wp_faqp_id' => 'ID: ' . $post->ID ), $actions );
		}
		
		return $actions;
	}

	/**
	 * Add Save button to Faq listing page
	 * 
	 * @package WP FAQ Pro
	 * @since 1.1.5
	 */
	function wp_faqp_restrict_manage_posts(){

		global $typenow, $wp_query;

		if( $typenow == 'sp_faq' && isset($wp_query->query['orderby']) && $wp_query->query['orderby'] == 'menu_order title' ) {

			$html  = '';
			$html .= "<span class='spinner sp-faq-spinner'></span>";
			$html .= "<input type='button' name='sp_faq_save_order' class='button button-secondary right sp-faq-save-order' id='sp-faq-save-order' value='".__('Save Sort Order', 'sp-faq')."' />";
			echo $html;
		}
	}

	/**
	 * Update Faq order
	 * 
	 * @package WP FAQ Pro
	 * @since 1.1.5
	 */
	function wp_faqp_update_post_order() {

		// Taking some defaults
		$result 			= array();
		$result['success'] 	= 0;
		$result['msg'] 		= __('Sorry, Something happened wrong.', 'sp-faq');

		if( !empty($_POST['form_data']) ) {

			$form_data 		= parse_str($_POST['form_data'], $output_arr);
			$sp_faq_posts 	= !empty($output_arr['sp_faq_post']) ? $output_arr['sp_faq_post'] : '';

			if( !empty($sp_faq_posts) ) {

				$post_menu_order = 0;

				// Loop od ids
				foreach ($sp_faq_posts as $sp_faq_key => $sp_faq_post) {
					
					// Update post 37
					$update_post = array(
						'ID'           => $sp_faq_post,
						'menu_order'   => $post_menu_order,
						);

					// Update the post into the database
					wp_update_post( $update_post );

					$post_menu_order++;
				}

				$result['success'] 	= 1;
				$result['msg'] 		= __('Faq order saves successfully.', 'sp-faq');
			}
		}

		echo json_encode($result);
		exit;
	}

	/**
	 * Function to add extra plugins link
	 * 
	 * @package WP FAQ Pro
	 * @since 1.1.5
	 */
	function wp_faqp_plugin_row_meta( $links, $file ) {
		
		if ( $file == WP_FAQP_BASENAME ) {
			
			$license_url = add_query_arg( array('page' => 'faqpro-license'), admin_url('plugins.php') );
			
			$row_meta = array(
				'docs'    => '<a href="' . esc_url('https://www.wponlinesupport.com/pro-plugin-document/document-wp-faq-pro/') . '" title="' . esc_attr( __( 'View Documentation', 'sp-faq' ) ) . '" target="_blank">' . __( 'Docs', 'sp-faq' ) . '</a>',
				'support' => '<a href="' . esc_url('https://www.wponlinesupport.com/welcome-wp-online-support-forum/') . '" title="' . esc_attr( __( 'Visit Customer Support Forum', 'sp-faq' ) ) . '" target="_blank">' . __( 'Support', 'sp-faq' ) . '</a>',
				);
			return array_merge( $links, $row_meta );
		}
		return (array) $links;
	}
	
	/**
	 * Add `FAQ` tab to Woocommerce Product Page at admin side
	 * 
	 * @package WP FAQ Pro
	 * @since 1.1.7
	 */
	function wp_faqp_woo_product_data_tabs( $data_tabs ) {

		$data_tabs['wp_faqp_data'] = array(
										'label'  => __( 'FAQ PRO', 'sp-faq' ),
										'target' => 'wp_faqp_product_data',
										'class'  => array( 'wp-faqp-data' ),
									);

		return $data_tabs;
	}
	
	/**
	 * Function to save metabox values
	 * 
	 * @package WP FAQ Pro
	 * @since 1.0.0
	 */
	function wp_faqp_save_woo_metabox_value( $post_id ) {

		global $post_type;
		
		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )                	// Check Autosave
		|| ( ! isset( $_POST['post_ID'] ) || $post_id != $_POST['post_ID'] )  	// Check Revision
		|| ( $post_type !=  'product' ) )			              				// Check if current post type is supported.
		{
		  return $post_id;
		}

		$prefix = WP_FAQP_META_PREFIX; // Taking metabox prefix

		// Taking variables
		$faqp_enable 	= isset($_POST[$prefix.'enable']) 		? 1 : 0;
		$faqp_shortcode	= isset($_POST[$prefix.'shortcode'])	? wp_faqp_slashes_deep($_POST[$prefix.'shortcode']) : '';

		update_post_meta($post_id, $prefix.'enable', $faqp_enable);
		update_post_meta($post_id, $prefix.'shortcode', $faqp_shortcode);
	}
}

$wp_faqp_admin = new Wp_Faqp_Admin();