<?php
/**
 * Settings Page
 *
 * @package WP FAQ Pro
 * @since 1.1.7
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
?>

<div class="wrap wp-faqp-settings">

<h2><?php _e( 'WP FAQ Pro Settings', 'sp-faq' ); ?></h2><br />

<?php

// Success message
if( isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true' ) {
	echo '<div id="message" class="updated notice notice-success is-dismissible">
			<p><strong>'.__("Your changes saved successfully.", "sp-faq").'</strong></p>
		  </div>';
}
?>

<form action="options.php" method="POST" id="wp-faqp-settings-form" class="wp-faqp-settings-form">
	
	<?php
	    settings_fields( 'wp_faqp_plugin_options' );
	    global $wp_faqp_options;
	?>

	<!-- General Settings Starts -->
	<div id="wp-faqp-general-sett" class="post-box-container wp-faqp-general-sett">
		<div class="metabox-holder">
			<div class="meta-box-sortables ui-sortable">
				<div id="general-css" class="postbox">

					<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>

						<!-- Settings box title -->
						<h3 class="hndle">
							<span><?php _e( 'General Settings', 'sp-faq' ); ?></span>
						</h3>
						
						<div class="inside">
						
						<table class="form-table wp-faqp-general-sett-tbl">
							<tbody>
								<tr>
									<th scope="row">
										<label for="wp-faqp-enable-woo-faq"><?php _e('Enable WooCommerce FAQ', 'sp-faq'); ?>:</label>
									</th>
									<td>
										<input type="checkbox" name="wp_faqp_options[enable_woo_faq]" class="wp-faqp-enable-woo-faq" id="wp-faqp-enable-woo-faq" value="1" <?php checked(wp_faqp_get_option('enable_woo_faq'), 1); ?> /><br/>
										<span class="description"><?php _e('Check this box to enable FAQ for WooCommerce.', 'sp-faq'); ?></span>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="wp-faqp-woo-faq-text"><?php _e('WooCommerce FAQ Tab Text', 'sp-faq'); ?>:</label>
									</th>
									<td>
										<input type="text" name="wp_faqp_options[woo_faq_tab_text]" class="wp-faqp-woo-faq-text regular-text" id="wp-faqp-woo-faq-text" value="<?php echo wp_faqp_esc_attr(wp_faqp_get_option('woo_faq_tab_text')); ?>" /><br/>
										<span class="description"><?php _e('Enter WoCommerce FAQ tab name. Default is FAQ.', 'sp-faq'); ?></span>
									</td>
								</tr>
								<tr>
									<td colspan="2" valign="top" scope="row">
										<input type="submit" id="wp-faqp-settings-submit" name="wp-faqp-settings-submit" class="button button-primary right" value="<?php _e('Save Changes','sp-faq'); ?>" />
									</td>
								</tr>
							</tbody>
						 </table>

					</div><!-- .inside -->
				</div><!-- #general-css -->
			</div><!-- .meta-box-sortables ui-sortable -->
		</div><!-- .metabox-holder -->
	</div><!-- #wp-faqp-general-sett -->
	<!-- General Settings Ends -->

	<!-- Custom CSS Settings Starts -->
	<div id="wp-faqp-custom-css-sett" class="post-box-container wp-faqp-custom-css-sett">
		<div class="metabox-holder">
			<div class="meta-box-sortables ui-sortable">
				<div id="custom-css" class="postbox">

					<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>

						<!-- Settings box title -->
						<h3 class="hndle">
							<span><?php _e( 'Custom CSS Settings', 'sp-faq' ); ?></span>
						</h3>
						
						<div class="inside">
						
						<table class="form-table wp-faqp-custom-css-sett-tbl">
							<tbody>
								<tr>
									<th scope="row">
										<label for="wp-faqp-custom-css"><?php _e('Custom CSS', 'sp-faq'); ?>:</label>
									</th>
									<td>
										<textarea name="wp_faqp_options[custom_css]" class="large-text wp-faqp-custom-css" id="wp-faqp-custom-css" rows="15"><?php echo wp_faqp_esc_attr(wp_faqp_get_option('custom_css')); ?></textarea><br/>
										<span class="description"><?php _e('Enter custom CSS to override plugin CSS.', 'sp-faq'); ?></span>
									</td>
								</tr>
								<tr>
									<td colspan="2" valign="top" scope="row">
										<input type="submit" id="wp-faqp-settings-submit" name="wp-faqp-settings-submit" class="button button-primary right" value="<?php _e('Save Changes','sp-faq'); ?>" />
									</td>
								</tr>
							</tbody>
						 </table>

					</div><!-- .inside -->
				</div><!-- #custom-css -->
			</div><!-- .meta-box-sortables ui-sortable -->
		</div><!-- .metabox-holder -->
	</div><!-- #wp-faqp-custom-css-sett -->
	<!-- Custom CSS Settings Ends -->

</form><!-- end .wp-faqp-settings-form -->

</div><!-- end .wp-faqp-settings -->