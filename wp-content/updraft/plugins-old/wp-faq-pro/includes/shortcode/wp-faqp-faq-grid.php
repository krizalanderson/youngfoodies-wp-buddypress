<?php
/**
 * Shortcode File
 *
 * Handles the 'faq-category-grid' shortcode of plugin
 *
 * @package WP FAQ Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * 'faq-category-grid' shortcode
 * 
 * @package WP FAQ Pro
 * @since 1.0.0
 */
function wp_faqp_grid_shortcode( $atts, $content = null ) {

	// Shortcode Parameter
	extract(shortcode_atts(array(
			'limit' 			=> '20',
			'grid'     			=> '1',
			'border_color'		=> '',
			'background_color' 	=> '',
			'font_color'		=> '',
			'font_size' 		=> '',
			'order'				=> 'DESC',
			'orderby'			=> 'date',
			'exclude_post'		=> array(),
			'posts'				=> array(),
			'cat_orderby'		=> 'name',
			'cat_order'			=> 'ASC',
			'cat_limit'			=> '',
			'exclude_cat'		=> array(),
			'cats'				=> array(),
			'display_post_count'=> false,
			'post_to_show'		=> 2,
			'show_more_button'	=> '',
			'show_less_button' 	=> ''
		), $atts));
	
	// Enqueing required script
	wp_enqueue_script( 'wp-faqp-public-script' );
	
	$unique 			= wp_faqp_get_unique();
	$limit				= !empty($limit) 				? $limit : '20';
	$background_color	= !empty($background_color) 	? $background_color : '#eee';
	$border_color		= !empty($border_color) 		? $border_color 	: '#d9d9d9';
	$font_color			= !empty($font_color) 			? $font_color 		: '#444';
	$font_size			= !empty($font_size) 			? $font_size 		: '15';
	$order 				= ( strtolower($order) == 'desc') ? 'DESC' 			: 'ASC';
	$orderby			= !empty($orderby) 			? $orderby 				: 'date';
	$exclude_post 		= !empty($exclude_post)		? explode(',', $exclude_post) 	: array();
	$posts 				= !empty($posts)			? explode(',', $posts) 			: array();
	$cat_orderby		= !empty($cat_orderby) 		? $cat_orderby 	: 'name';
	$cat_order 			= ( strtolower($cat_order) == 'desc') ? 'DESC' : 'ASC';
	$cat_limit			= !empty($cat_limit) 		? $cat_limit : '';
	$exclude_cat 		= !empty($exclude_cat)		? explode(',', $exclude_cat) 	: array();
	$cats 				= !empty($cats)				? explode(',', $cats) 	: array();
	$display_post_count = ($display_post_count == 'true') ? true : false;
	$post_to_show		= !empty($post_to_show) ? $post_to_show : 2;
	$show_more_button	= !empty($show_more_button) ? $show_more_button : __( 'Show More', 'sp-faq' );
	$show_less_button	= !empty($show_less_button) ? $show_less_button : __( 'Show Less', 'sp-faq' );
	
	// Taking some variables
	$faqgrid = wp_faqp_grid_column( $grid );
	
	ob_start();
		
		// FAQ unique style
		$style = '';
		$style .= "<style type='text/css'>";

				$style .= ".faq-pro-grid-view-{$unique} .faq-grid-inner{";
					if( !empty($background_color) ) {
						$style .= "background:{$background_color};";
					}

					if( !empty($border_color) ) {
						$style .= "border:1px solid {$border_color};";
					}

					if( !empty($font_color) ) {
						$style .= "color:{$font_color};";
					}
				$style .= "} ";
				
				$style .= ".faq-pro-grid-view-{$unique} .faq-grid-inner a{";
					if( !empty($font_color) ) {
						$style .= "color:{$font_color};";
					}
					if( !empty($font_size) ) {
						$style .= "font-size:{$font_size}px;}";
					}
				$style .= "} ";

		$style .= "</style>";
		echo $style; // echo FAQ style
 		
 		$faq_cat_args = array(
 								'taxonomy' 	=> WP_FAQP_CAT,
 								'orderby' 	=> $cat_orderby,
 								'order' 	=> $cat_order,
 								'number'	=> $cat_limit,
 								'exclude'	=> $exclude_cat,
 								'include'	=> $cats,
 							);
	$terms = get_terms( $faq_cat_args );

	if( !empty($terms) && !is_wp_error($terms) ) {

		$term_count 		= count($terms);
		$i 					= 1;
		$count 				= 0;
	?>

		<div class="wp-faqp-grid-wrp wp-faqp-clearfix">

	<?php	
		// Loop of terms
		foreach($terms as $term) {

			// WP Query Parameter
			$args = array (
							'post_type' 		=> WP_FAQP_POST_TYPE,
							'post_status'		=> array( 'publish' ),
							'orderby'			=> $orderby,
							'order' 			=> $order,
							'posts_per_page' 	=> $limit,
							'post__not_in'		=> $exclude_post,
							'post__in'			=> $posts,
					);
			
			// Category Parameter
			if( $term->term_id != "" ) {
				$args['tax_query'] = array(
										array(
											'taxonomy' 	=> WP_FAQP_CAT,
											'field' 	=> 'term_id',
											'terms' 	=> $term->term_id
									));
			}
			
			// WP Query
			$query 		= new WP_Query($args);
			$post_count = $query->post_count;

			$count++;
			$post_loop_count 	= 1;
			$css_class 			= "faqgridclear";
			
			if ( ( is_numeric( $grid ) && ( $grid > 0 ) && ( 0 == ($count - 1) % $grid ) ) || 1 == $count ) { $css_class .= ' first'; }
			if ( ( is_numeric( $grid ) && ( $grid > 0 ) && ( 0 == $count % $grid ) ) || $term_count == $count ) { $css_class .= ' last'; }

			// If post is there
			if( $query->have_posts() ) {
			?>
			
			<div class="faq-pro-grid-view-<?php echo $unique; ?> faq-grid-view spfp-medium-<?php echo $faqgrid; ?> <?php echo $css_class; ?> spfp-wpcolumns">
				<div class="faq-grid-inner">

					<h2 class="faq-cat-name">
						<?php echo $term->name;

						if( $display_post_count ) {
							echo ' - ('.$post_count.')';
						} ?>
					</h2>

					<div class="wp-faqp-list-wrp show-hide-main">

						<ul class="wp-faqp-list" id="faqlist-<?php echo $i; ?>">

						<?php
						while ( $query->have_posts() ) : $query->the_post();
							$hide_cls = ( $post_loop_count > $post_to_show ) ? 'wp-faqp-hide' : '';
						?>

							<li class="faq-list-item <?php echo $hide_cls; ?>">
								<div class="faq-grid-title">
									<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title();?></a>
								</div>
							</li>

						<?php
							$post_loop_count++;
							endwhile;
						?>

						</ul><!-- #faqlist -->

						<?php if( ($post_count > $post_to_show) ) { ?>
							<div class="wp-faqp-more faq-button"><a href="javascript:void(0);"><?php echo $show_more_button; ?></a></div>
							<div class="wp-faqp-hide wp-faqp-less faq-button" data-show-faq="<?php echo $post_to_show; ?>"><a href="javascript:void(0);"><?php echo $show_less_button; ?></a></div>
						<?php } ?>

					</div><!-- end .show-hide-main -->
				</div><!-- end .faq-grid-inner -->
			</div><!-- end .faq-grid-view -->

			<?php } // end of have_posts() ?>

			<?php $i++;
		}
		?>
		
		</div><!-- end .wp-faqp-grid-wrp -->

	<?php
	} else {

		// WP Query Parameters
		$args = array (
						'post_type'      	=> WP_FAQP_POST_TYPE,
						'post_status'		=> array( 'publish' ),
						'orderby'        	=> $orderby, 
						'order'          	=> $order,
						'posts_per_page' 	=> $limit,
						'post__not_in'		=> $exclude_post,
						'post__in'			=> $posts,
					);

		// Category Parameter
		if( !empty($exclude_cat) ) {
			
			$args['tax_query'] = array(
										array(
											'taxonomy' 	=> WP_FAQP_CAT,
											'field' 	=> 'term_id',
											'terms' 	=> $exclude_cat,
											'operator'	=> 'NOT IN'
											));
		}

		// WP Query
		$query = new WP_Query($args);

		// If post is there
		if( $query->have_posts() ) {
	?>
		
		<div class="wp-faqp-grid-view faq-grid-view">
			<div class="faq-grid-inner">

				<div class="show-hide-main">

				<?php while ( $query->have_posts() ) : $query->the_post(); ?>

				<div class="faq-grid-title">
					<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title();?></a>
				</div>

				<?php endwhile; ?>

				</div><!-- end .show-hide-main -->

			</div><!-- end .faq-grid-inner -->
		</div><!-- end .wp-faqp-grid-view -->

	<?php
		} // End of check have posts

	} // End of else

	wp_reset_query(); // Reset WP Query

	$content .= ob_get_clean();
	return $content;
}

// 'faq-category-grid' shortcode
add_shortcode('faq-category-grid', 'wp_faqp_grid_shortcode');