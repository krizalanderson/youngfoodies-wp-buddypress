<?php
/**
 * The Header for your theme.
 *
 * Displays all of the <head> section and everything up until <div id="main">
 *
 * @package WordPress
 * @subpackage OneSocial Theme
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="msapplication-tap-highlight" content="no"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <!--		 <link href="http://youngfoodies.co.uk/wp-content/themes/onesocial-child/css/custom.css" rel="stylesheet">-->
    <link href="<?php echo get_bloginfo('url') . '/wp-content/themes/onesocial-child/css/custom.css'; ?>"
          rel="stylesheet">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css"/>

    <link rel="te-custom-css" href="<?php echo get_stylesheet_directory_uri() . "/css/te_custom1.css"; ?>"/>
    <link rel="te-custom-css" href="<?php echo get_stylesheet_directory_uri() . "/css/te_custom2.css"; ?>"/>
    <script>window.onload = function () {
            setTimeout(function () {
                scrollTo(0, 0);
            }, 100); //100ms for example
        }</script>

    <?php wp_head(); ?>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-96666196-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<?php $inputs = (onesocial_get_option('boss_inputs')) ? '1' : '0'; ?>

<body <?php body_class('home-page'); ?> data-inputs="<?php echo $inputs; ?>">

<?php do_action('buddyboss_before_header'); ?>

<?php get_template_part('template-parts/mobile-right-panel'); ?>

<div id="main-wrap">

    <?php do_action('onesocial_before_header'); ?>

    <header id="masthead" class="site-header"
            data-infinite="<?php echo (onesocial_get_option('boss_activity_infinite')) ? 'on' : 'off'; ?>">
        <div class="header-wrapper">
            <?php get_template_part('template-parts/header-logo'); ?>
            <!--            --><?php //get_template_part('template-parts/header-nav'); ?>
            <?php get_template_part('template-parts/header-aside'); ?>
        </div>
    </header>

    <?php get_template_part('template-parts/header-mobile'); ?>

    <?php do_action('buddyboss_after_header'); ?>

    <?php
    $show_single_header = apply_filters('onesocial_single_header', (is_single() && !(function_exists('is_bbpress') && is_bbpress()) && !(function_exists('is_product') && is_product())));
    if ($show_single_header) {
        get_template_part('template-parts/header-single');
    }
    ?>


    <!--home--header-->

    <div class="home_header">
        <div class="container">
            <?php
            /*
             * Logo Option
             */

            $show = onesocial_get_option('logo_switch');
            $logo_id = onesocial_get_option('boss_logo', 'id');
            $site_title = get_bloginfo('name');
            $logo = ($show && $logo_id) ? wp_get_attachment_image($logo_id, 'medium', '', array('class' => 'boss-logo')) : $site_title;

            // This is for better SEO
            $elem = (is_front_page() && is_home()) ? 'h1' : 'h2';
            ?>

            <div id="logo-area">

                <<?php echo $elem; ?> class="site-title">

                <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                    <?php echo $logo; ?>
                </a>

            </<?php echo $elem; ?>>

            <p class="site-description"><?php bloginfo('description'); ?></p>

        </div>
        <div class="home-login">
            <ul class="te-header-custom-menu">

                <li class="custom-menu-item"><a class="<?php if(is_home() || is_page('home')) echo "active"; ?>" href="<?php echo get_bloginfo('url'); ?>">home</a></li>
                <li class="custom-menu-item"><a class="<?php if(is_page('package')) echo "active"; ?>" href="<?php echo get_bloginfo('url'); ?>/package">package</a></li>
                <li class="custom-menu-item"><a class="<?php if(is_page('pricing')) echo "active"; ?>" href="<?php echo get_bloginfo('url'); ?>/pricing">pricing</a></li>

                <?php

                if (is_user_logged_in()) {

                    $current_user = wp_get_current_user();

                    echo '<li style="margin-left:50px;"><a class="back-to-site" href="/newsfeed/"> ' . get_avatar($current_user->user_email, 60) . ' <br> To site </a></li>';
                    //echo $current_user->user_firstname ;

                } else {
                    echo '<li style="margin-left:50px;"><a href="#" class="login header-button animatedClick boss-tooltip" id="home_login" data-target="LoginBox" data-tooltip="Log in">LOGIN</a></li>';
                }


                ?>
            </ul>
        </div>
    </div>
</div>
<!--endhomeheader-->

<div id="inner-wrap" class="homepage-inner-wrap">

    <?php
    if (function_exists('yoast_breadcrumb') && !is_home() && !is_front_page()) {
        yoast_breadcrumb('<div class="breadcrumb-wrapper"><p id="breadcrumbs">', '</p></div>');
    }
    ?>

    <?php do_action('buddyboss_inside_wrapper'); ?>

    <?php global $post; ?>

    <div id="page"
         class="<?php echo (is_single() && get_post_type($post->ID) == 'post' && has_post_thumbnail($post->ID)) ? 'has-thumbnail ' : ''; ?>hfeed site">

        <div id="main" class="wrapper">
					
					
				