<?php
get_header(); 
?>
<?php
global $post;
?>
<div class="event_single">
<?php
$EM_Event = em_get_event($post->ID, 'post_id');  ?>
<div class="event_title">
<?php echo $EM_Event->output('#_EVENTNAME'); ?>
</div>
<div class="event_thumbnail">
<?php echo get_the_post_thumbnail(); ?>
</div>
<div class="time_loc_day">
<div class="time_loc_date">
<?php
echo $EM_Event->output('#_EVENTDATES'); ?>
</div>
<div class="time_loc_time">
<?php
echo $EM_Event->output('#_EVENTTIMES');
?></div>
<div class="time_loc_loce">
<?php
echo $EM_Event->output('#_LOCATIONNAME, #_LOCATIONTOWN, #_LOCATIONPOSTCODE, #_LOCATIONCOUNTRY');


?>
</div>
<div class="time_loc_loce time_loc_price">
<?php
echo $EM_Event->output('#_EVENTPRICERANGE'); ?>
</div>

<a target="_blank" href="<?php echo $EM_Event->output('#_EVENTGCALURL'); ?>">Add to google cal</a>
<a target="_blank" href="<?php echo $EM_Event->output('#_EVENTICALURL'); ?>">Add to iCal</a>



</div>
<div class="event_des">

<h2>Description of event</h2>
<?php
echo $EM_Event->output('#_EVENTEXCERPT');
?>
</div>
<div class="event_atnd">

<span>
<h2>Who’s attending?</h2>
<?php 
echo $EM_Event->output('#_BOOKINGBUTTON'); ?>
</span>
<?php
echo $EM_Event->output('#_ATTENDEES');
echo $EM_Event->output('#_ATTENDEESLIST');


 ?>

</div>


<div class="publised_by">
<h2>Published by</h2>
<?php echo $EM_Event->output('#_CONTACTUSERNAME'); ?>
</div>


<?php
get_footer();
?>
