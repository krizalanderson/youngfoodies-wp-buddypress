<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage OneSocial Theme
 */
?>
</div><!-- #main .wrapper -->
</div><!-- #page -->
</div> <!-- #inner-wrap -->

<!-- Don't delete this -->
<div class="bb-overlay"></div>

</div><!-- #main-wrap (Wrap For Mobile) -->

<?php do_action( 'buddyboss_before_footer' ); ?>

<?php
global $bp, $post;

$post_infinite			 = onesocial_get_option( 'post_infinite' );
$boss_activity_infinite	 = onesocial_get_option( 'boss_activity_infinite' );
$activity				 = isset( $bp ) ? $bp->current_component : false;

// don't remove this filter, marketplace plugin uses it
$show_footer = apply_filters( 'onesocial_show_footer', !( ( is_archive() || is_home() ) && $post_infinite && !( function_exists( 'is_shop' ) && is_shop() ) && !( function_exists( 'is_product_category' ) && is_product_category() ) && !( function_exists( 'is_product_tag' ) && is_product_tag() ) && !( function_exists( 'bbp_is_forum_archive' ) && bbp_is_forum_archive() ) ) || ( $activity == 'activity' && $boss_activity_infinite ) );

if ( $show_footer ) {

	$style = onesocial_get_option( 'onesocial_footer' );
	?>
	
	<div class="home_footer">
	 
		        <div class="footer-inner">

		            <div id="footer-links">

						<?php
						$show_copyright	 = onesocial_get_option( 'footer_copyright_content' );
						$copyright		 = onesocial_get_option( 'boss_copyright' );

						if ( $show_copyright && $copyright ) {
							?>

							<div class="footer-credits <?php if ( !has_nav_menu( 'secondary-menu' ) ) : ?>footer-credits-single<?php endif; ?>">
								<?php echo $copyright; ?>
							</div>

						<?php } ?>

						<?php if ( has_nav_menu( 'secondary-menu' ) ) : ?>
							<ul class="footer-menu">
								<?php wp_nav_menu( array( 'container' => false, 'menu_id' => 'nav', 'theme_location' => 'secondary-menu', 'items_wrap' => '%3$s', 'depth' => 1, ) ); ?>
							</ul>
						<?php endif; ?>

		            </div>

					<?php if ( onesocial_get_option( 'boss_layout_switcher' ) ) { ?>
						<form id="switch-mode" name="switch-mode" method="post" action="">
							<input type="submit" value="View as Desktop" tabindex="1" id="switch_submit" name="submit" />
							<input type="hidden" id="switch_mode" name="switch_mode" value="desktop" />
							<?php wp_nonce_field( 'switcher_action', 'switcher_nonce_field' ); ?>
						</form>
					<?php } ?>

					<?php get_template_part( 'template-parts/footer-social-links' ); ?>

				</div><!-- .footer-inner -->

		    

		
	</div>

	<footer id="colophon" class="<?php echo $style; ?>">

		<?php get_template_part( 'template-parts/footer', 'widgets' ); ?>

		<?php if ( 'footer-style-1' == $style ): ?>

		    <div class="footer-inner-bottom">
		        <div class="footer-inner">

		            <div id="footer-links">

						<?php
						$show_copyright	 = onesocial_get_option( 'footer_copyright_content' );
						$copyright		 = onesocial_get_option( 'boss_copyright' );

						if ( $show_copyright && $copyright ) {
							?>

							<div class="footer-credits <?php if ( !has_nav_menu( 'secondary-menu' ) ) : ?>footer-credits-single<?php endif; ?>">
								<?php echo $copyright; ?>
							</div>

						<?php } ?>

						<?php if ( has_nav_menu( 'secondary-menu' ) ) : ?>
							<ul class="footer-menu">
								<?php wp_nav_menu( array( 'container' => false, 'menu_id' => 'nav', 'theme_location' => 'secondary-menu', 'items_wrap' => '%3$s', 'depth' => 1, ) ); ?>
							</ul>
						<?php endif; ?>

		            </div>

					<?php if ( onesocial_get_option( 'boss_layout_switcher' ) ) { ?>
						<form id="switch-mode" name="switch-mode" method="post" action="">
							<input type="submit" value="View as Desktop" tabindex="1" id="switch_submit" name="submit" />
							<input type="hidden" id="switch_mode" name="switch_mode" value="desktop" />
							<?php wp_nonce_field( 'switcher_action', 'switcher_nonce_field' ); ?>
						</form>
					<?php } ?>

					<?php get_template_part( 'template-parts/footer-social-links' ); ?>

				</div><!-- .footer-inner -->

		    </div><!-- .footer-inner-bottom -->

		<?php endif; ?>

		<!-- Don't delete this -->
		<div class="bb-overlay"></div>

	</footer>

<?php } ?>

<?php
if ( !is_user_logged_in() ) {

	if ( onesocial_get_option( 'user_login_option' ) ) {

		if ( buddyboss_is_bp_active() && bp_get_signup_allowed() ) {
			get_template_part( 'template-parts/site-register' );
		}

		get_template_part( 'template-parts/site-login' );
	}
}

// Lost Password
get_template_part( 'template-parts/site-lost-password' );
?>

<?php do_action( 'bp_footer' ) ?>
 <script>
 if (jQuery('#nav-bar-filter #activity-personal-li').length > 0) 
 
 if (jQuery("#nav-bar-filter #activity-personal-li, #events-personal-li").hasClass("current")){
    jQuery('#item-body .item-list-tabs ul').css('display', 'none');
}


  $(function() {
        $('#colorselector').change(function(){
            $('.colors').hide();
            $('#' + $(this).val()).slideDown();
        });
    });


 	var founder_cost = [];
 	founder_cost[0] = 0;
 	founder_cost[1] = 700;
 	founder_cost[2] = 520;
 	founder_cost[3] = 520;
 	founder_cost[4] = 460;
 	founder_cost[5] = 460;
 	founder_cost[6] = 460;
 	founder_cost[7] = 460;
 	founder_cost[8] = 460;
 	founder_cost[9] = 460;
 	founder_cost[10] = 460;
 	founder_cost[11] = 460;
 	founder_cost[12] = 460;
 	founder_cost[13] = 460;
 	founder_cost[14] = 460;
 	founder_cost[15] = 460;


 	var team_member_cost = [];
 	team_member_cost[0] = 0;
 	team_member_cost[1] = 600;
 	team_member_cost[2] = 420;
 	team_member_cost[3] = 420;
 	team_member_cost[4] = 360;
 	team_member_cost[5] = 360;
 	team_member_cost[6] = 360;
 	team_member_cost[7] = 360;
 	team_member_cost[8] = 360;
 	team_member_cost[9] = 360;
 	team_member_cost[10] = 360;
 	team_member_cost[11] = 360;
 	team_member_cost[12] = 360;
 	team_member_cost[13] = 360;
 	team_member_cost[14] = 360;
 	team_member_cost[15] = 360;



 $("#number_of_founders").keyup(function() {
	 var number_of_founders = $('#number_of_founders').val();
	 var number_of_team_members = $('#number_of_team_members').val();


	 if (number_of_founders == ''){
		 number_of_founders = 0;
	 }

	 if (number_of_team_members == ''){
		 number_of_team_members = 0;
	 }

	 number_of_founders = Number(number_of_founders);
	 number_of_team_members = Number(number_of_team_members);

//	 alert(number_of_founders);

	 if (number_of_founders > 15)
		 founder_cost[number_of_founders] = 460;

	 if (number_of_team_members > 15)
		 team_member_cost[number_of_team_members] = 360;

	 total_fouder_cost = number_of_founders*founder_cost[number_of_founders];
	 total_team_member_cost = number_of_team_members*team_member_cost[number_of_team_members];

	 total_cost = total_fouder_cost + total_team_member_cost;

	 $("#founder_cost").html(total_fouder_cost);
	 $("#team_cost").html(total_team_member_cost);
	 $("#total_cost").html(total_cost);

 });

 $("#number_of_team_members").keyup(function() {
	 var number_of_founders = $('#number_of_founders').val();
	 var number_of_team_members = $('#number_of_team_members').val();

	 if (number_of_founders == ''){
		 number_of_founders = 0;
	 }

	 if (number_of_team_members == ''){
		 number_of_team_members = 0;
	 }

	 number_of_founders = Number(number_of_founders);
	 number_of_team_members = Number(number_of_team_members);

//	 alert(number_of_founders);

	 if (number_of_founders > 15)
		 founder_cost[number_of_founders] = 460;

	 if (number_of_team_members > 15)
		 team_member_cost[number_of_team_members] = 360;

//	 alert(number_of_founders);
	 total_fouder_cost = number_of_founders*founder_cost[number_of_founders];
	 total_team_member_cost = number_of_team_members*team_member_cost[number_of_team_members];

	 total_cost = total_fouder_cost + total_team_member_cost;


	 $("#founder_cost").html(total_fouder_cost);
	 $("#team_cost").html(total_team_member_cost);

	 $("#total_cost").html(total_cost);

 });





 $(".page-numbers").on("click", function() {
    $(window).scrollTop(0);  
});	
</script>
<script type="text/javascript">
	$(document).on('click','.forgot_password',function(){
	     $(".forgetme").addClass("highlight");
	});

</script>
<?php wp_footer(); ?>
<style>body .bb-global-search-ac li.bbls-category {border-bottom:none !important;} .bboss_ajax_search_item .item .item-desc {display:none !important;}</style>
</body>
</html>