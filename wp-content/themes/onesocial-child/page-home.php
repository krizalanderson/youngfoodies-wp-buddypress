<?php
/**
 * The template for displaying WordPress pages, including HTML from BuddyPress templates.
 *
 * @package WordPress
 * @subpackage OneSocial Theme
 * @since OneSocial Theme 1.0.0
 */
get_header('home');
?>

    <div id="primary" class="site-content default-page">

        <div id="content" role="main">

            <div class="homepage-banner">
                <div class="banner-heading">
                    <h4>A community of inspiring people bringing delicious products to life.</h4>
                    <h4>Small Brands. Big Thinking.</h4>
                </div>
                <div class="join-community">
                    <?php echo do_shortcode('[mc4wp_form id="3890"]'); ?>
                </div>
            </div>

            <div class="te-block">
                <h3 style="margin-top: 0px !important; padding: 15px 0px;">Our key ingredient: Knowledge</h3>
            </div>
            <div class="homepage-intro">


                <div class="intro-text">
                    <p>Young Foodies is a members’ portal for inspiring individuals from start-up grocery brands. Our
                        fundamental belief is that, if we make it easier to deal with challenges like labelling,
                        forecasting and legals, our members will have the breathing space to do what really matters –
                        make the best products possible.</p>
                    <p>We provide knowledge through specialist blogs and training and we’re creating an online
                        community, so that our members can build real friendships with those in the same boat as them.
                        Everything we do is aimed at making the journey from start-up food brand to fully-fledged
                        business easier.</p>
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="area-of-strength">
                <h3 style="margin-bottom: 0px;">Our areas of strength</h3>


                <div style="background-color: #f3f3f4;" class="row">
                    <p class="padding-adjust">Although we aim to help in all areas of the business, we focus on the areas that we believe will
                        benefit most from pooling resources and knowledge. At the moment these include:</p>
                        <div  style="margin:25px 0;">
                    <div class="col-sm-4">
                        <div class="strength-box s-box-1">
                            <div class="strength-box-overlay">
                                <h4>Founders</h4>
                                <ul>
                                    <li>People management</li>
                                    <li>Investment strategy</li>
                                    <li>EMI schemes</li>
                                    <li>Business strategic planning…</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="strength-box s-box-2">
                            <div class="strength-box-overlay">
                                <h4>Operations</h4>
                                <ul>
                                    <li>Forecasting demand</li>
                                    <li>Negotiating contracts</li>
                                    <li>Managing distribution</li>
                                    <li>Dealing with raw materials…</li>
                                </ul>
                            </div>
                        </div>  
                    </div>
                    <div class="col-sm-4">
                        <div class="strength-box s-box-3">
                            <div class="strength-box-overlay">
                                <h4>Product</h4>
                                <ul> 
                                    <li>Building NPD processes</li>
                                    <li>Dealing with factories</li>
                                    <li>Finding new suppliers</li>
                                    <li>Microbiological analysis…</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    </div>
                    <p style="text-align: center;">Plus HR and recruitment, systems and technology, insurance, finance,
                        legal and IP and more…</p>
                        <style type="text/css">
                                .our-partners {
                                    margin-top:50px !important;
                                }
                                .our-partners img{
                                    width:120px;
                                }
                        </style>
                    <div class="our-partners" >
                        <div class="row">
                            <div class="col-sm-2">
                                <p style="line-height: 86px;text-align: center;">Working with...</p>
                            </div>
                            <div class="col-sm-2">
                                <img class="img-partner img-responsive"
                                     src="<?php echo get_stylesheet_directory_uri() . '/images/partners/BS.png' ?>"/>
                            </div>
                            <div class="col-sm-2">
                                <img class="img-partner"
                                     src="<?php echo get_stylesheet_directory_uri() . '/images/partners/Poet.png' ?>"/>
                            </div>
                            <div class="col-sm-2">
                                <img class="img-partner"
                                     src="<?php echo get_stylesheet_directory_uri() . '/images/partners/PM.png' ?>"/>
                            </div>
                            <div class="col-sm-2">
                                <img class="img-partner"
                                     src="<?php echo get_stylesheet_directory_uri() . '/images/partners/TW.png' ?>"/>
                            </div>
                            <div class="col-sm-2">
                                <img class="img-partner"
                                     src="<?php echo get_stylesheet_directory_uri() . '/images/partners/BD.png' ?>"/>
                            </div>
                        </div>
                    </div>
                </div>


            </div>


            <style>
                .our-community table.slider-table{
                    width:95%;
                }
                .our-community table.slider-table tr td
                {
                    border-bottom: none;
                    padding:0px;
                    font-size: 16px !important;
                }
                .our-community table.slider-table tr td:first-child{
                    width:45%;
                }
            </style>

            <div class="our-community">
                <h3>our community is wonderful</h3>
                <div class="slider responsive community-slider">
                    <div>
                        <div class="row">

                          
                          
                            <table class="slider-table">

                                <tbody>

                                <tr>
                                    <td>
                                        <img src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Pipnut.png' ?>">
                                    </td>
                                    <td>"Young Foodies is the missing piece in tying together an inspirational
                                        community for the benefits of all."
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                          
                            <table class="slider-table">

                                <tbody>

                                <tr>
                                    <td><img
                                        src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Coco.png' ?>">
                                    </td>
                                    <td>"Absolutely love the idea and am so excited by all the opportunities
                                        that lie ahead."</td>
                                </tr>

                                </tbody>

                            </table>
                        </div>
                    </div>
                    <div>
                        <div class="row">

                            <table class="slider-table">

                                <tbody>

                                <tr>
                                    <td>
                                        <img src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Mallow.png' ?>">
                                    </td>
                                    <td>"For once all of the networking opportunities aren’t just for sales or
                                        marketing. Being able to bounce ideas off others in the industry is invaluable."</td>
                                </tr>

                                </tbody>

                            </table>
                        </div>
                    </div>
                    <div>
                        <div class="row">

                           

                            <table class="slider-table">

                                <tbody>

                                <tr>
                                    <td>
                                        <img src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Propercorn.png' ?>">
                                    </td>
                                    <td>"It’s high time that the small players in the food industry club
                                        together and enable themselves to one day become the big players whilst retaining their
                                        passion and dynamism."
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div>
                        <div class="row">

                        
                            <table class="slider-table">

                                <tbody>

                                <tr>
                                    <td>
                                        <img src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Savse4.png' ?>">
                                    </td>
                                    <td>"It's reassuring that others are in same boat with similar challenges
                                        and open to collaboration for mutual benefit."
                                    </td>
                                </tr>

                                </tbody>
                                </table>
                        </div>
                    </div>
                    <div>
                        <div class="row">

                            <table class="slider-table">

                                <tbody>

                                <tr>
                                    <td>
                                        <img src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Ugly.png' ?>">
                                    </td>
                                    <td>"Community is the key to making the UK a global hub for food & drink.
                                        Young Foodies is bringing people together, and I love that."
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div>
                        <div class="row">

                            <table class="slider-table">

                                <tbody>

                                <tr>
                                    <td>
                                        <img src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/UpGo.png' ?>">
                                    </td>
                                    <td>"Brilliant! We all face the same challenges, we all undertake the same
                                        learning process. Collective knowledge will add infinite value."
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

<style type="text/css">

.our-features a.our-features-btn {
    width: 150px;
    float: none;
    margin: 0 auto;
    background: #ff765d !important;
    color: #fff;
    display: block;
    padding: 12px;
    font-size: 16px;
    text-align: center;
    border:none !important;
}
.our-features a.our-features-btn:hover
{    outline: none;
    color: #fff;
    text-decoration: none;
    -moz-box-shadow: inset 0 0 1000px rgba(0,0,0,.25);
    -webkit-box-shadow: inset 0 0 1000px rgba(0,0,0,.25);
    box-shadow: inset 0 0 1000px rgba(0,0,0,.25);
}}
</style>

            <div class="our-features">

                <a href="<?php echo get_bloginfo('url').'/package/'?>" class="pt-pb-button our-features-btn">Our Package</a>
                <div class="join-community">
                    <?php echo do_shortcode('[mc4wp_form id="3890"]'); ?>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    <script>
        jQuery(document).ready(function () {


            jQuery('.mc4wp-form-fields input').focus(function(){
                jQuery(this).attr("placeholder", "");
            });

            jQuery('.mc4wp-form-fields input').blur(function(){
                jQuery(this).attr("placeholder", "Request to join with your email address");
            });

            jQuery('.community-slider').slick({
                dots: true,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });

        })
    </script>
<?php

get_sidebar();

get_footer();