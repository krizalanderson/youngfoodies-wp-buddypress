<?php
/**
 * The template for displaying WordPress pages, including HTML from BuddyPress templates.
 *
 * @package WordPress
 * @subpackage OneSocial Theme
 * @since OneSocial Theme 1.0.0
 */
get_header('home');
?>

    <div id="primary" class="site-content default-page">

        <div id="content" role="main">

            <div class="homepage-banner">
                <div class="banner-heading">
                    <h4>A community of inspiring people bringing delicious products to life.</h4>
                    <h4>Small Brands. Big Thinking.</h4>
                </div>
                <div class="join-community">
                    <?php echo do_shortcode('[mc4wp_form id="3890"]'); ?>
                </div>
            </div>

            <div class="homepage-intro">
                <div class="intro-text">
                    <p>Young Foodies is a members’ portal for inspiring individuals from start-up grocery brands. Our
                        fundamental belief is that, if we make it easier to deal with challenges like labelling,
                        forecasting and legals, our members will have the breathing space to do what really matters –
                        make the best products possible.</p>
                    <p>We provide knowledge through specialist blogs and training and we’re creating an online
                        community, so that our members can build real friendships with those in the same boat as them.
                        Everything we do is aimed at making the journey from start-up food brand to fully-fledged
                        business easier.</p>
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="area-of-strength">
                <h3>Our areas of strength</h3>
                <p>Although we aim to help in all areas of the business, we focus on the areas that we believe will
                    benefit most from pooling resources and knowledge:</p>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="strength-box s-box-1">
                            <div class="strength-box-overlay">
                                <h4>Founders</h4>
                                <ul>
                                    <li>People management</li>
                                    <li>Investment strategy</li>
                                    <li>EMI schemes</li>
                                    <li>Business strategic planning…</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="strength-box s-box-2">
                            <div class="strength-box-overlay">
                                <h4>Operations</h4>
                                <ul>
                                    <li>Forecasting demand</li>
                                    <li>Negotiating contracts</li>
                                    <li>Managing distribution</li>
                                    <li>Dealing with raw materials…</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="strength-box s-box-3">
                            <div class="strength-box-overlay">
                                <h4>Product</h4>
                                <ul>
                                    <li>Building NPD processes</li>
                                    <li>Dealing with factories</li>
                                    <li>Finding new suppliers</li>
                                    <li>Microbiological analysis…</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <p style="text-align: center;">(Plus HR and recruitment, systems and technology, insurance, finance.
                    legal and IP and more…)</p>
            </div>

            <div class="our-partners">
                <div class="row">
                    <div class="col-sm-2">
                        <p style="line-height: 120px;text-align: center;">Working with...</p>
                    </div>
                    <div class="col-sm-2">
                        <img class="img-partner"
                             src="<?php echo get_stylesheet_directory_uri() . '/images/partners/PicTaylor.png' ?>"/>
                    </div>
                    <div class="col-sm-2">
                        <img class="img-partner"
                             src="<?php echo get_stylesheet_directory_uri() . '/images/partners/PicPM.png' ?>"/>
                    </div>
                    <div class="col-sm-2">
                        <img class="img-partner"
                             src="<?php echo get_stylesheet_directory_uri() . '/images/partners/PicBethan.png' ?>"/>
                    </div>
                    <div class="col-sm-2">
                        <img class="img-partner"
                             src="<?php echo get_stylesheet_directory_uri() . '/images/partners/PicP.png' ?>"/>
                    </div>
                    <div class="col-sm-2">
                        <img class="img-partner"
                             src="<?php echo get_stylesheet_directory_uri() . '/images/partners/PicWiser.png' ?>"/>
                    </div>
                </div>
            </div>


            <div class="our-community">
                <h3>some of our community</h3>
                <div class="slider responsive community-slider">
                    <div>
                        <div class="row">
                            <div class="col-sm-4"><img
                                        src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Coco.png' ?>">
                            </div>
                            <div class="col-sm-8">"Absolutely love the idea and am so excited by all the opportunities
                                that lie ahead."
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-sm-4"><img
                                        src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Mallow.png' ?>">
                            </div>
                            <div class="col-sm-8">"For once all of the networking opportunities aren’t just for sales or
                                marketing. Being able to bounce ideas off others in the industry is invaluable."
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-sm-4"><img
                                        src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Nutribrex.png' ?>">
                            </div>
                            <div class="col-sm-8">"Community is the key to making the UK a global hub for food & drink.
                                Young Foodies is bringing people together, and I love that."
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-sm-4"><img
                                        src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Packd.png' ?>">
                            </div>
                            <div class="col-sm-8">"Community is the key to making the UK a global hub for food & drink.
                                Young Foodies is bringing people together, and I love that."
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-sm-4"><img
                                        src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Pipnut.png' ?>">
                            </div>
                            <div class="col-sm-8">"Young Foodies is the missing piece in tying together an inspirational
                                community for the benefits of all."
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-sm-4"><img
                                        src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Propercorn.png' ?>">
                            </div>
                            <div class="col-sm-8">"It’s high time that the small players in the food industry club
                                together and enable themselves to one day become the big players whilst retaining their
                                passion and dynamism."
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-sm-4"><img
                                        src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Proteinball.png' ?>">
                            </div>
                            <div class="col-sm-8">"Community is the key to making the UK a global hub for food & drink.
                                Young Foodies is bringing people together, and I love that."
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-sm-4"><img
                                        src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Savse4.png' ?>">
                            </div>
                            <div class="col-sm-8">"It's reassuring that others are in same boat with similar challenges
                                and open to collaboration for mutual benefit."
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-sm-4"><img
                                        src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/Ugly.png' ?>">
                            </div>
                            <div class="col-sm-8">"Community is the key to making the UK a global hub for food & drink.
                                Young Foodies is bringing people together, and I love that."
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-sm-4"><img
                                        src="<?php echo get_stylesheet_directory_uri() . '/images/community-slider/UpGo.png' ?>">
                            </div>
                            <div class="col-sm-8">"Brilliant! We all face the same challenges, we all undertake the same
                                learning process. Collective knowledge will add infinite value."
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div class="our-features">

                <button type="button" class="pt-pb-button our-features-btn">Our Features</button>
                <div class="join-community">
                    <?php echo do_shortcode('[mc4wp_form id="3890"]'); ?>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    <script>
        jQuery(document).ready(function () {

            jQuery('.community-slider').slick({
                dots: true,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });

        })
    </script>
<?php

get_sidebar();

get_footer();