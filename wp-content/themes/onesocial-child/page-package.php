<?php
/**
 * The template for displaying WordPress pages, including HTML from BuddyPress templates.
 *
 * @package WordPress
 * @subpackage OneSocial Theme
 * @since OneSocial Theme 1.0.0
 */
get_header('home');
?>

    <div id="primary" class="site-content default-page">

        <div id="content" role="main">

            <div class="member-benefits te-block">
                <h3>We're incredibly proud of our membership package</h3>
            </div>

            <div class="feature-block left-feature first-block">
                <div class="feature-text ">
                    <h3>Join an Inspiring Community</h3>
                    <div style="clear: both;"></div>
                    <p>Our community is a place for like-minded people to ask questions, share advice and inspire each
                        other. There are plenty of opportunities to connect, from forums and private messaging to
                        sharing stories at an event. We encourage our members to make the most of this community - it's
                        where the most value is shared.</p>
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="feature-block right-feature second-block">
                <div class="feature-text ">
                    <h3>Attend our member events</h3>
                    <div style="clear: both;"></div>
                    <p>Whether through classroom training courses, interactive workshops or online webinars, members
                        attend our events to become masters in food and drink.
                    </p>
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="feature-block left-feature third-block">
                <div class="feature-text ">
                    <h3>Access relevant knowledge</h3>
                    <div style="clear: both;"></div>
                    <p>We've partnered with a selection of geniuses who know legal, product, finance and all those
                        difficult subjects like the back of their hand. They write regular blogs and publish useful
                        templates on their specialisms so our members can sleep easier at night.</p>
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="feature-block right-feature forth-block">
                <div class="feature-text ">
                    <h3>The icing on the cake…</h3>
                    <div style="clear: both;"></div>
                    <p>Our community gives us strength in numbers, so we can negotiate member deals from a powerful
                        position. This has allowed us to secure brilliant discounts across all areas of the business
                        such as recruitment, design and legal costs.
                    </p>
                </div>
                <div style="clear: both;"></div>
            </div>


<style type="text/css">

.our-prices a.our-prices-btn {
    width: 150px;
    float: none;
    margin: 0 auto;
    background: #ff765d !important;
    color: #fff;
    display: block;
    padding: 12px;
    font-size: 16px;
    text-align: center;
    border:none !important;
}
.our-prices a.our-prices-btn:hover
{    outline: none;
    color: #fff;
    text-decoration: none;
    -moz-box-shadow: inset 0 0 1000px rgba(0,0,0,.25);
    -webkit-box-shadow: inset 0 0 1000px rgba(0,0,0,.25);
    box-shadow: inset 0 0 1000px rgba(0,0,0,.25);
}}
</style>
  

            <div class="our-prices">

                <a href="<?php echo get_bloginfo('url').'/pricing/'?>"  class="pt-pb-button our-prices-btn">Our Pricing</a>
            </div>

        </div>
    </div>

<?php

get_sidebar();

get_footer();