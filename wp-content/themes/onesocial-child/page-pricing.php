<?php
/**
 * The template for displaying WordPress pages, including HTML from BuddyPress templates.
 *
 * @package WordPress
 * @subpackage OneSocial Theme
 * @since OneSocial Theme 1.0.0
 */
get_header('home');
?>

    <div id="primary" class="site-content default-page">

        <div id="content" role="main">

            <div class="criteria-for-membership te-block">
                <h3 style="margin-top: 0px !important; padding: 15px 0px;">Our five Criteria for Membership</h3>
                <div class="criteria-img">
                    <img style="width:100%;"
                         src="<?php echo get_stylesheet_directory_uri() . '/images/170617Criterialist.png' ?>"/>
                </div>
            </div>

            <div class="types-of-membership te-block">
                <h3>Our two types of Membership</h3>

                <p class="content-in-type-of-membership-section" style="margin-bottom: 0px !important;">Founder Membership includes more sensitive content suitable for the leaders of the brand.</p>
                <div class="type-of-membership-wrapper">
                    <div class="row">


                        <div class="col-sm-offset-1 col-sm-5">
                            <div class="membership-section">
                                <div class="top-orange-area">
                                   <p class="content-in-TOM-section-title">Team member</p>
                                   <p class="content-in-TOM-section-title">membership</p>
                                </div>
                                <div class="middle-white-area">
                                    <p class="content-in-TOM-section" style="font-weight: 600;">From £360 + vat</p>
                                    <p class="content-in-TOM-section" style="font-size: 12px !important;">per member per year</p>
                                </div>
                                <div class="bottom-grey-ish-section">
                                    <p class="content-in-TOM-section-lowest-box">Unlimited access to blogs</p>
                                    <p class="content-in-TOM-section-lowest-box">Invitation to all events</p>
                                    <p class="content-in-TOM-section-lowest-box">Full access to the community</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="membership-section">
                                <div class="top-orange-area">
                                    <p class="content-in-TOM-section-title">Founder</p>
                                    <p class="content-in-TOM-section-title">membership</p>
                                </div>
                                <div class="middle-white-area">
                                    <p class="content-in-TOM-section" style="font-weight: 600;">From £460 + vat</p>
                                    <p class="content-in-TOM-section" style="font-size: 12px !important;">per member per year</p>
                                </div>
                                <div class="bottom-grey-ish-section">
                                    <p class="content-in-TOM-section-lowest-box">All Team Member benefits</p>
                                    <p class="content-in-TOM-section" style="color: #ff765d;">PLUS</p>
                                    <p class="content-in-TOM-section-lowest-box">Access to specialist <i>founder-only</i> blogs</p>
                                    <p class="content-in-TOM-section-lowest-box">Invitations to specialist <i>founder-only</i> events</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <p class="white-line">It's cheaper when lots of your team join at the same time...</p>
            </div>



            <div class="price-calculator">

                <div class="price-table-section">

                    <table cellpadding="5px" class="price-table">
                        <thead>
                        <tr>
                            <td>Total # of people*</td>
                            <td class="text-center">1</td>
                            <td class="text-center">2-3</td>
                            <td class="text-center">4</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Per team member</td>
                            <td class="text-center" style="font-weight: 600;">£600</td>
                            <td class="text-center" style="font-weight: 600;">£420</td>
                            <td class="text-center" style="font-weight: 600;">£360</td>
                        </tr>
                        <tr>
                            <td>Per founder</td>
                            <td class="text-center" style="font-weight: 600;">£700</td>
                            <td class="text-center" style="font-weight: 600;">£520</td>
                            <td class="text-center" style="font-weight: 600;">£460</td>
                        </tr>
                        </tbody>
                    </table>



                </div>

                <div class="calculator-overlay">
                    <div class="top-heading">
                        <h3 style="width: 100% !important;">CALCULATE YOUR total price</h3>
                    </div>
                    <div class="row calculator-top-row">
                        <div class="col-xs-4">
                            <div class="calculator-box">
                                <h5># founders</h5>
                                <input type="text" value="0" class="founders_count calculator-input"/>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="calculator-box">
                                <h5># team members</h5>
                                <input type="text" value="0" class="team_members_count calculator-input"/>


                            </div>
                        </div>

                        <div class="col-xs-4">

                        </div>
                    </div>

                    <div class="row second-row-calculator">
                        <div class="col-xs-4">
                            <div class="calculator-box">

                                <h5>Per Founder</h5>
                                <h3 class="per_founder_value">£<span>0</span></h3>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="calculator-box">

                                <h5>Per Team Member</h5>
                                <h3 class="per_team_member_value">£<span>0</span></h3>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="calculator-box">
                                <div class="calculator-output">
                                    <h5>Total ex VAT</h5>
                                    <h3 class="calculator_output_value">£<span>0</span><sup>*</sup></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div style="background-color: #f3f3f4;" class="row">
                <div>

                    
<style type="text/css">

.contact-us-button a {
    width: 150px;
    float: none;
    margin: 0 auto;
    background: #ff765d !important;
    color: #fff;
    display: block;
    padding: 12px;
    font-size: 16px;
    text-align: center;
    border:none !important;
}
.contact-us-button a:hover
{    outline: none;
    color: #fff;
    text-decoration: none;
    -moz-box-shadow: inset 0 0 1000px rgba(0,0,0,.25);
    -webkit-box-shadow: inset 0 0 1000px rgba(0,0,0,.25);
    box-shadow: inset 0 0 1000px rgba(0,0,0,.25);
}}
</style>


                    <div class="contact-us-button">
                        <a href="mailto:hello@youngfoodies.co.uk?Subject=More%20info%20about%20Young%20Foodies" target="_top" style="    color: #fff;
    font-size: 16px;
    padding: 20px 30px !important;
    width: 300px;
">Contact us for more info</a>
                    </div>

                    <div class="join-community">
                        <?php echo do_shortcode('[mc4wp_form id="3890"]'); ?>
                        <div style="clear: both;"></div>
                    </div>


                    <div class="calculator-bottom-note"><sup>*</sup> You must apply at the same time to benefit from
                        group discounts.
                    </div>

                </div>

            </div>


        </div>
    </div>

    <script>




        function number_format (number, decimals, decPoint, thousandsSep) {

            number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
            var n = !isFinite(+number) ? 0 : +number
            var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
            var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
            var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
            var s = ''
            var toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec)
                return '' + (Math.round(n * k) / k)
                        .toFixed(prec)
            }
            // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || ''
                s[1] += new Array(prec - s[1].length + 1).join('0')
            }
            return s.join(dec)
        }



        var for_1_member = {founder: 700, team_member: 600};
        var for_2_3_member = {founder: 520, team_member: 420};
        var for_4_member = {founder: 460, team_member: 360};

        function calculate_founders_team_members_fee() {
            var founders = jQuery('.founders_count').val();
            var team_members = jQuery('.team_members_count').val();

            founders = parseInt(founders);
            team_members = parseInt(team_members);

            founders = isNaN(founders) ? 0 : founders;
            team_members = isNaN(team_members) ? 0 : team_members;

            var total_ = founders + team_members;

            var per_founder = 0;
            var per_team_member = 0;
            if (total_ == 1) {
                per_founder = for_1_member.founder;
                per_team_member = for_1_member.team_member;
            } else if (total_ >= 2 && total_ <= 3) {
                per_founder = for_2_3_member.founder;
                per_team_member = for_2_3_member.team_member;
            } else if (total_ >= 4) {
                per_founder = for_4_member.founder;
                per_team_member = for_4_member.team_member;
            }


            jQuery('.per_founder_value span').html(per_founder);
            jQuery('.per_team_member_value span').html(per_team_member);

            var total_value = founders * per_founder + team_members * per_team_member;

            jQuery('.calculator_output_value span').html(number_format(total_value));
        }

        jQuery(document).ready(function () {

            jQuery('.founders_count').focus(function(){
                jQuery(this).css("border", "2px solid black");
            });

            jQuery('.founders_count').blur(function(){
                jQuery(this).css("border", "2px solid #ff765d");

            });

            jQuery('.team_members_count').focus(function(){
                jQuery(this).css("border", "2px solid black");
            });

            jQuery('.team_members_count').blur(function(){
                jQuery(this).css("border", "2px solid #ff765d");
            });

            jQuery('.mc4wp-form-fields input').focus(function(){
                jQuery(this).attr("placeholder", "");
            });

            jQuery('.mc4wp-form-fields input').blur(function(){
                jQuery(this).attr("placeholder", "Request to join with your email address");
            });


            calculate_founders_team_members_fee();

            jQuery('body').on('input change', '.calculator-input', function (e) {

                calculate_founders_team_members_fee();

            });



            jQuery('.community-slider').slick({
                dots: true,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });

        })
    </script>
<?php

get_sidebar();

//echo "here";exit;

get_footer('');