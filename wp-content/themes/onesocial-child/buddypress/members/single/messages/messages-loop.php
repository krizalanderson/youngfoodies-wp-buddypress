<?php
do_action( 'bp_before_member_messages_loop' ); ?>

	<?php do_action( 'bp_before_member_messages_threads' ); 
	global $messages_template;
	?>

	<form action="<?php echo bp_loggedin_user_domain() . bp_get_messages_slug() . '/' . bp_current_action() ?>/bulk-manage/" method="post" id="messages-bulk-management">
	
		<div class="messages-options-nav">
			<?php bp_messages_bulk_management_dropdown(); ?>
		</div><!-- .messages-options-nav -->

		<?php wp_nonce_field( 'messages_bulk_nonce', 'messages_bulk_nonce' ); ?>
		
		<table id="message-threads" class="messages-table">
		
			<thead>
				<tr>
                    <th scope="col" class="thread-checkbox"><input id="select-all-messages" type="checkbox"><strong></strong></th>
<?php if ( 'sentbox' != bp_current_action() ) {
	?>
	<th style="text-align: left !important;" scope="col" class="thread-from"><?php _e( 'From', 'buddypress' ); ?></th>

<?php
	}
	else{?>

		<th style="text-align: left !important;" scope="col" class="thread-from"><?php _e( 'To', 'buddypress' ); ?></th>

		<?php
	}

 ?>


					<th scope="col" class="thread-info"><?php _e( 'Subject', 'buddypress' ); ?></th>
					<th scope="col" class="thread-date"><?php _e( 'Date', 'buddypress' ); ?></th>
					<th style="text-align: left !important;" scope="col" class="thread-options"><?php _e( 'Actions', 'buddypress' ); ?></th>
                    <?php

					/**
					 * Fires inside the messages box table header to add a new column.
					 *
					 * This is to primarily add a <th> cell to the messages box table header. Use
					 * the related 'bp_messages_inbox_list_item' hook to add a <td> cell.
					 *
					 * @since BuddyPress (2.3.0)
					 */
					do_action( 'bp_messages_inbox_list_header' ); ?>

					<?php if ( bp_is_active( 'messages', 'star' ) ) : ?>
						<th scope="col" class="thread-star"></th>
					<?php endif; ?>
				</tr>
			</thead>
			
			<tbody> 
    
    
<?php if(isset($_POST['action']) && isset($_POST['search_terms'])) : 

$user_id = get_current_user_id();
global $wpdb;
$result = $wpdb->get_results( "	SELECT a . * , b.user_nicename
								FROM  `wp_bp_messages_recipients` AS a
								JOIN  `wp_users` AS b ON a.user_id = b.ID
								AND a.user_id =1
								AND a.sender_only =0
								AND a.is_deleted =0
								LIMIT 0 , 30 "); 

    echo 	'<tr id="m-13" class=" unread-message">
    			<td colspan="5" style="width: 100%!important;">
    				<h2 class="second_head">Seach Result by Member: <small>('.$_POST['search_terms'].')</small></h2> 
				</td>
			</tr>';
$aa=1;
foreach($result as $row){


/*
id
thread_id
sender_id
subject
message
date_sent
user_nicename
*/

	$fetch_user_sql = 	"SELECT a.* , b.display_name
						FROM  `wp_bp_messages_messages` AS a
						JOIN wp_users AS b ON a.thread_id =$row->thread_id
						AND a.sender_id !=1
						AND a.sender_id = b.ID
						LIMIT 0 ,1";

	$fetch_user = $wpdb->get_results( $fetch_user_sql ); 


	if( !preg_match( '/'.$_POST['search_terms'].'/i', $fetch_user[0]->display_name ) ) continue;


    echo 	'<tr id="m-' .$row->thread_id. '" >
    			<td><input type="checkbox" name="message_ids[]" class="message-check styled" value="' .$row->thread_id. '"> 
    			</td>
				<td class="thread-from">
					<div class="table">
	                	<div class="table-cell">
	                	<a href="#">' . $fetch_user[0]->display_name . '</a>
	                	</div>
                    </div>
                </td>
				<td class="thread-info">
					<p><a href="http://www.youngfoodies.co.uk/members/youngfoodies/messages/view/' .$row->thread_id. '/" title="View Message">' .$fetch_user[0]->subject .'</a></p>
					<p class="thread-excerpt">' .$fetch_user[0]->message .'</p>
				</td>
				<td class="thread-date">
					<span class="activity"> '.date("M d", strtotime( $fetch_user[0]->date_sent )) .'</span>
				</td>
				<td>
				<a href="'.wp_nonce_url( trailingslashit( bp_loggedin_user_domain() . bp_get_messages_slug() . '/' . bp_current_action() . '/delete/' . $row->thread_id ), 'messages_delete_thread' ).'">delete</a>
					</td>
			</tr>';
			$aa++;
}

?>

<?php if($aa == 1){ ?>
	<tr>
		<td colspan="5" class="color_inner">
			<div id="message" class="info" style="    float: left!important;    position: static;">
				<p>Sorry, no users were found.</p>
			</div>
		</td>
	</tr>
<?php } ?>

<?php
    	echo '<tr id="m-13" class=" unread">
    			<td colspan="5" style="width: 100%!important;">
    				<h2 class="second_head">Seach Result by Messages: <small>('.$_POST['search_terms'].')</small></h2>
				</td>
			</tr>';

?>

<?php endif; ?>

<?php if ( bp_has_message_threads( bp_ajax_querystring( 'messages' ) ) ) { ?>

	<style>

		#buddypress table#message-threads tr td:first-child, #buddypress table#message-threads td.thread-date{
			margin-top: 25px !important;
		}

		.my-messages td.thread-info{
			margin-top: 0px !important;
		}

		#buddypress table#message-threads:not(.messages-notices) tr td:last-child{
			margin-top: 25px ;
		}

		.unread-message{
			background: #fbfbfb !important;
		}

		.messages #buddypress table{
			border: 0px !important;
		}

		#buddypress table#message-threads tr td:first-child
		{
			/*margin-top: 25px;*/
		}
		#buddypress table#message-threads td.thread-info{
			/*margin-top: 32px;*/
		}
		#buddypress table#message-threads .thread-from{
			/*margin-top: 21px;*/

		}

		#buddypress table thead tr{

			border-bottom: 2px solid #ff765d !important;
		}

		.message-action-star span.icon:before, .message-action-unstar span.icon:before {

			font-size: 12px !important;
		}

	</style>
				<?php 

				while ( bp_message_threads() ) : bp_message_thread(); ?>

					<tr id="m-<?php bp_message_thread_id(); ?>" class="<?php bp_message_css_class(); ?><?php if ( bp_message_thread_has_unread() ) : ?> unread-message<?php else: ?> <b>Read</b><?php endif; ?>">
						<td style="margin-top: 25px !important;">
                            <input type="checkbox" name="message_ids[]" class="message-check" value="<?php bp_message_thread_id(); ?>" /><strong></strong>
						</td>

						<?php if ( 'sentbox' != bp_current_action() ) : ?>
							<td class="thread-from">
                                <div style="display: inline-flex; margin-top: 15px; margin-bottom: 15px;" class="table">
                                    <div  class="table-cell"><?php bp_message_thread_avatar( array( 'width' => 60, 'height' => 60 ) ); ?></div>
                                    <div class="table-cell" style="margin-top: 13px; width:120px !important;"><?php bp_message_thread_from(); ?></div>
                                </div>
								<?php //bp_message_thread_total_and_unread_count(); ?>
							</td>
						<?php else: ?>
							<td class="thread-from">
                                <div style="display: inline-flex; margin-top: 15px; margin-bottom: 15px;"  class="table">
                                    <div style="margin-top: 10px;" class="table-cell"><?php bp_message_thread_avatar( array( 'width' => 60, 'height' => 60 ) ); ?></div>
                                    <div class="table-cell"  style="margin-top: 13px; width:120px !important;" ><?php bp_message_thread_to(); ?></div>
                                </div>
								<?php //bp_message_thread_total_and_unread_count(); ?>
							</td>
						<?php endif; ?>

						<td class="thread-info">
							<p style="margin-top: 25px;"><a href="<?php bp_message_thread_view_link(); ?>" title="<?php esc_attr_e( "View Message", "onesocial" ); ?>"><?php bp_message_thread_subject(); ?></a></p>
<!--							<p class="thread-excerpt">--><?php //bp_message_thread_excerpt(); ?><!--</p>-->
						</td>
						<?php

						 /* Fires inside the messages box table row to add a new column.
						 *
						 * This is to primarily add a <td> cell to the message box table. Use the
						 * related 'bp_messages_inbox_list_header' hook to add a <th> header cell.
						 *
						 * @since BuddyPress (1.1.0)
						 */
						do_action( 'bp_messages_inbox_list_item' ); ?>
						<td class="thread-date">
							<span class="activity"><?php  echo buddyboss_format_time(strtotime( bp_get_message_thread_last_post_date_raw() )); ?></span>
						</td>

						<td style="display: inline-flex !important;" class="thread-options">
							<?php if ( bp_message_thread_has_unread() ) : ?>
								<a style="margin-right: 2px;" class="read" href="<?php bp_the_message_thread_mark_read_url(); ?>"><?php _e( 'Read', 'onesocial' ); ?></a> |
							<?php else : ?>
								<a style="margin-right: 2px; margin-left: 2px;" class="unread" href="<?php bp_the_message_thread_mark_unread_url(); ?>"><?php _e( 'Unread', 'onesocial' ); ?></a> |
							<?php endif; ?>

							<a style="margin-right: 2px; margin-left: 2px;" class="delete" href="<?php bp_message_thread_delete_link(); ?>"><?php _e( 'Delete', 'onesocial' ); ?></a> |

							<?php if ( bp_is_active( 'messages', 'star' ) ) : ?>
								<div style="margin-bottom: 2px !important;" class="thread-star">
									<?php
									if ( function_exists( 'bp_the_message_star_action_link' ) ) {
										bp_the_message_star_action_link( array( 'thread_id' => bp_get_message_thread_id() ) );
									}
									?>
								</div>
							<?php endif; ?>
						</td>
					</tr>

				<?php endwhile; ?>



			<?php }else{ ?>
<td colspan="5" class="color_inner"> 
		<div id="message" class="info" style="    float: left!important;    position: static;">
			<p>Sorry, no messages were found.</p>
		</div>
	</td>
	<?php } ?>

			</tbody>

		</table><!-- #message-threads -->

	</form>

	<?php do_action( 'bp_after_member_messages_threads' ); ?>
	
	<!--<div class="pagination no-ajax" id="user-pag">-->
	<div id="pag-bottom" class="pagination no-ajax">

		<div class="pag-count" id="messages-dir-count">
			<?php bp_messages_pagination_count(); ?>
		</div>

		<div class="pagination-links" id="messages-dir-pag">
			<?php bp_messages_pagination(); ?>
		</div>

	</div><!-- .pagination -->

	<?php do_action( 'bp_after_member_messages_pagination' ); ?>

	<?php do_action( 'bp_after_member_messages_options' ); ?>


<?php do_action( 'bp_after_member_messages_loop' ); ?>