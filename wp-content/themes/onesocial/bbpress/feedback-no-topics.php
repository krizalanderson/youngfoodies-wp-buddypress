<?php

/**
 * No Topics Feedback Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div class="bbp-template-notice">
	<p><?php _e( 'No topics so far - add one below to get the conversation started...', 'bbpress' ); ?></p>
</div>
